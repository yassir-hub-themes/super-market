@if($cart_items = app('cart')->getContent())
    <section class="content-section cart-content">
        <div class="container">
            <h2 class="page-title">@lang("Items")</h2>
            @include('theme::checkout.cart.items')
        </div>
        <hr>
        @include('theme::checkout.cart.discounts-inputs')
        <hr>
        <div id="cart-total-summation-container" class="d-flex justify-content-center">
        @include(Ecommerce::view('cart-total-summation'))
    </section>
@endif
