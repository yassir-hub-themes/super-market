<div class="container">
    <div class="totals">
        <div class="total">
            <span class="total-title">@lang("Subtotal")</span>
            <span class="total-number">
                 {{ Ecommerce::formatPrice(Cart::getSubTotal()) }} {{Ecommerce::currentSymbol()}}
            </span>
        </div>
        @foreach(Cart::getConditions() as $condition)
            <div class="total">
                    <span class="total-title">
                      @lang(ucfirst($condition->getType()))
                    </span>
                <span class="total-number">
                    @if(preg_match('/%/', $condition->getValue()))
                        {{$condition->getValue()}}
                    @else
                        {{Ecommerce::formatPrice($condition->getValue())}}
                        {{Ecommerce::currentSymbol()}}
                    @endif
                </span>
            </div>
        @endforeach
        <div class="total">
                    <span class="total-title">
                        @lang("Total")
                    </span>
            <span class="total-number">{{ Ecommerce::formatPrice(Cart::getTotal()) }} {{Ecommerce::currentSymbol()}}</span>
        </div>
    </div>
</div>
