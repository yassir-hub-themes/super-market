<div class="cart-items">
    @foreach($cart_items as $item)
        <div class="cart-item">
            <a href="{{route("shop.show",[Str::plural($item->associatedModel->type),$item->associatedModel->id])}}"
               class="loading-img cart-img">
                <img class="lazy-img" src="{{$item->associatedModel->img_url }}">
            </a>
            <div class="cart-item-info">
                <a href="{{route("shop.show",[Str::plural($item->associatedModel->type),$item->associatedModel->id])}}"
                   class="cart-name font-weight-bold">
                    {{$item->name}}
                </a>
                <ul class="list-unstyled">
                    @foreach($item->attributes['options'] as $attribute)
                        <li class="item-option">
                            <span class="item-option-name">{{$attribute['name']}}</span>
                            <span class="item-option-separator"> : </span>
                            @if($attribute['type'] == 'image')
                                <a target="_blank"
                                   href="{{ upload_storage_url($attribute['value'])}}">@lang('File')</a>
                            @else
                                <span class="item-option-value">{{$attribute['value']}}</span>
                            @endif
                            @if($attribute['price'] != 0)
                                <span class="item-option-separator"> : </span>
                                <span class="item-option-price">{{Ecommerce::formatPrice($attribute['price'])}}</span>
                                <span class="item-option-price-symbol">{{Ecommerce::currentSymbol()}}</span>
                            @endif
                        </li>
                    @endforeach
                </ul>
                <div class="qty-price">
                    <div class="item-qty">
                        <a role="button" class="qty-control qty-plus">
                            <i class="fas fa-plus-circle"></i>
                        </a>
                        <a role="button" class="qty-control qty-minus">
                            <i class="fas fa-minus-circle"></i>
                        </a>
                        <input type="number" class="qty-input" value="{{$item->quantity}}"
                               @if($item->associatedModel->type !== "service" && $item->associatedModel->quantity > 0)
                               data-max="{{$item->associatedModel->quantity}}"
                               @endif
                               data-min="{{$item->associatedModel->min_quantity}}"
                               data-item="{{$item->associatedModel->id}}"
                               data-cart-id="{{$item->id}}"
                               data-has-ajax="true"
                        >
                    </div>
                    <strong class="item-price">
                        {{Ecommerce::formatPrice($item->getPriceWithConditions())}} {{Ecommerce::currentSymbol()}}
                    </strong>
                    <a role="button" class="item-delete" data-item="{{$item->id}}">
                        <i class="fas fa-times-circle"></i>
                    </a>
                </div>
            </div>
        </div>
    @endforeach
</div>
