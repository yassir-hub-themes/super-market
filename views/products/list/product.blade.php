<div class="product">
    <a href="{{route('shop.show',[Str::plural($product->type),$product->id])}}" class="product-img">
        <img src="{{$product->img_url}}"
             class="img-responsive">
    </a>
    <div class="product-content">
        <a class="product-name" href="#!">
            {{ $product->title }}
        </a>
        <div class="price-box">
            @if($product->hasOffer())
                <span class="first-price">
                        {{ Ecommerce::formatPrice($product->pricing->finalPrice()) }} {{Ecommerce::currentSymbol()}}
                </span>
                <span class="last-price">
                        {{ Ecommerce::formatPrice($product->pricing->originalPrice()) }} {{Ecommerce::currentSymbol()}}
                </span>
            @else
                <span class="first-price">
                        {{ Ecommerce::formatPrice($product->pricing->finalPrice()) }} {{Ecommerce::currentSymbol()}}
                </span>
            @endif
            @include('theme::products.product.unit-price',['product'=>$product])
        </div>
        @if($product->hasOffer())
            <span class="sale">%{{$product->pricing->offerRatio()}}</span>
        @endif
        <div class="qty-price">
            <div class="item-qty">
                <a role="button" class="qty-control qty-plus">
                    <i class="fas fa-plus-circle"></i>
                </a>
                <a role="button" class="qty-control qty-minus">
                    <i class="fas fa-minus-circle"></i>
                </a>
                <input type="number"
                       class="qty-input"
                       value="{{Cart::getQuantityByModelId($product->id)}}"
                       @if($product->type !== "service" && $product->quantity > 0)
                       data-max="{{$product->quantity}}"
                       @endif
                       data-item="{{$product->id}}"
                       data-min="{{$product->min_quantity}}"
                       data-has-ajax="true">
            </div>
        </div>
    </div>
</div>
