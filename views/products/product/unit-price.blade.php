@if($product->unit)
    <p class="price-sale-unit m-0">@lang('Price per :SALE_UNIT',['SALE_UNIT'=>$product->unit->sale_unit])</p>
    @if($product->pricing->mainUnitPrice() != $product->pricing->finalPrice())
        <p class="price-main-unit m-0">
            <?php
            $mainUnitPrice = $product->pricing->mainUnitPrice() . ' ' . Ecommerce::currentSymbol();
            ?>
            @lang(':PRICE / :MAIN_UNIT',['PRICE'=> $mainUnitPrice,'MAIN_UNIT'=>$product->unit->main_unit])
        </p>
    @endif
@endif
