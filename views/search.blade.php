@extends("theme::layouts.master")
@section('page_title',__("Shop - search"))
@section("content")
    <div class="content-section">
        <div class="container">
            <div class="row">
                <div id="content" class="col-sm-12">
                    <h1 class="content-head">@lang('Search - :query',['query'=>request()->get('term')])</h1>
                    <form action="{{route('shop.search')}}" id="search-form">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="float-field">
                                    <input type="text" placeholder="@lang('Search'):" name="term"
                                           value="{{request()->get('term')}}">
                                    <label>@lang('Search'):</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="float-field float-cont">
                                    <select name="category" data-placeholder="@lang("Select category")">
                                        @foreach($categories as $category)
                                            <option value=""></option>
                                            <option @if(request('category') == $category->id) selected
                                                    @endif value="{{$category->id}}">{{$category->title}}</option>
                                        @endforeach
                                    </select>
                                    <label>@lang('Category')</label>
                                </div>
                            </div>
                        </div>
                    </form>
                    <input type="submit" form="search-form" value="@lang("Search")" id="button-search"
                           class="btn btn-primary">
                    <h2 class="content-h2">@lang("Search results")</h2>
                    @if($items->count())
                        <div class="product-container">
                            @each('theme::products.list.product', $items, 'product')
                        </div>
                    @else
                        <h3>@lang("can't find :query",['query'=>request()->get('term')])</h3>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection
@push("styles.before")
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
@endpush

@push("scripts")
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>$("select").select2({
            allowClear: true
        });</script>
@endpush
