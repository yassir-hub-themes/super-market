@extends("theme::layouts.master")
@section('page_title',__('Checkout'))
@section("content")
    @include('theme::checkout.cart')
    @include('theme::components.checkout.form')
@endsection
@push("styles.before")
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <style>
        .select2.select2-container {
            width: 100% !important;
        }

        .delivery-time-content, .payment-methods-content {
            margin-top: 60px !important;
        }
    </style>

@endpush
@push("modals")
    @include('theme::components.checkout.modals.address-book')
@endpush
@push("scripts")
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>$(".select2").select2();</script>
    <template id="spinner">
        <div class="loader"></div>
    </template>
    <template id="empty-cart">
        <section class="content-section">
            <div class="container">
                <div class="status-flex lazy-head-img">
                    <div class="status-img loaded-img">
                        <i class="fas fa-shopping-basket" style=" font-size: 7rem; color: gray;"></i>
                    </div>
                    <h1 class="status">
                        @lang("Your cart is empty")
                    </h1>
                    <a href="{{route('home')}}" class="cart-btn">
                        @lang('Go to home')
                    </a>
                </div>
            </div>
        </section>
    </template>
    <script>
        $(function () {
            $(".cart-btn").on("click", function (e) {
                e.preventDefault()
                $('#checkout-store').submit();
            });
            @if(old('city_id'))
            setTimeout(() => {
                $(".city-select").change();
            }, 1000);
            @endif
        });

        $('.select-delivery').on('click', function () {
            let time = $("input[name='time-range']:checked");
            $('#delivery_time').val(time.val());
            $('#shift-id').val(time.data('shift-id'));
        });
        $('#immediately').click(function (e) {
            let now = new Date();
            $('#delivery_time').val(now.getHours() + ':' + now.getMinutes())
        });

        function updateDeliveryTimes(city) {
            $.ajax({
                url: route('ajax.city.times', city),
                success: function (data) {
                    applyDeliveryServiceByArea(city);
                    let opening_days = data.opening_days;
                    let defs = data.defs;

                    function disabled_days(date) {
                        return !Object.values(opening_days).includes(defs['_' + date.getDay()]);
                    }

                    $(".delivery-date-input").flatpickr({
                        locale: '{{app()->getLocale()}}',
                        minDate: new Date(data.firstAvailableInWeek),
                        dateFormat: "d M Y",
                        disable: [disabled_days],
                        defaultDate: @if(old('delivery_date')) '{{date('d M Y',strtotime(old('delivery_date')))}}'
                        @else new Date(data.firstAvailableInWeek) @endif,
                        onChange: function (selectedDates, dateStr, instance) {
                            $('#delivery_date').val(flatpickr.formatDate(selectedDates[0], "Y-m-d"));
                        },
                    });
                    $('#times-list').html(data.times_list)
                }
            })
        }

    </script>
@endpush

