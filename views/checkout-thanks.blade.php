@extends("theme::layouts.master")
@section('page_title',__('Home'))
@section("content")
    <section class="content-section status-content">
        <div class="container">
            <div class="status-flex">
                <div class="status-img loading-img">
                    <img class="lazy-img" data-src="{{Ecommerce::theme()->asset('images/delivery.png')}}">
                </div>
                <h1 class="status">
                    @lang('Your request is in progress')
                </h1>
{{--                <div class="status-period">--}}
{{--                    <h3 class="period-title">--}}
{{--                        @lang('The order will be delivered within')--}}
{{--                    </h3>--}}
{{--                    --}}{{--                    <span class="far fa-clock"></span>--}}
{{--                    --}}{{--                    <strong class="prriod-time">--}}
{{--                    --}}{{--                        {{ $time }}--}}
{{--                    --}}{{--                    </strong>--}}
{{--                </div>--}}
                <a href="{{ route('home') }}" class="cart-btn">
                    @lang('Back to home')
                </a>
            </div>
        </div>
    </section>
@endsection
