@extends("theme::layouts.master")
@section('page_title',__('Change password'))
@section("content")
    <section class="content-section my-acc-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="acc-head">
                        <h2 class="acc-title">
                            @lang('Change password')
                        </h2>
                    </div>
                    <form class="acc-form" action="{{ route('password.store') }}" method="POST">
                        @csrf
                        <div class="float-field">
                            @if (session('old_password'))
                                <span class="error-text-alert">
                                    {{ session('old_password') }}
                                </span>
                            @endif
                            @error('old_password')
                            <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                            @enderror
                            <input type="password" name="old_password" placeholder="@lang('Enter old password')">
                            <label>@lang('Enter old password')</label>
                        </div>
                        <div class="float-field">
                            @error('password')
                            <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                            @enderror
                            <input type="password" name="password" placeholder="@lang('Enter new password')">
                            <label>@lang('Enter new password')</label>
                        </div>
                        <div class="float-field">
                            @error('password_confirmation')
                            <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                            @enderror
                            <input type="password" name="password_confirmation" placeholder="@lang('Confirm password')">
                            <label>@lang('Confirm password')</label>
                        </div>
                        <button type="submit" class="submit-btn">
                            @lang('Change')
                        </button>
                    </form>
                </div>
                <aside id="column-right" class="col-sm-3 hidden-xs">
                    @include('theme::account.menu')
                </aside>

            </div>
        </div>
    </section>
@endsection
