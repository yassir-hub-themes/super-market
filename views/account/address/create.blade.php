@extends("theme::layouts.master")
@section('page_title',__('Add new address'))
@section("content")
    <section class="content-section address-book-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="acc-head">
                        <h2 class="acc-title">
                            @lang('Add new address')
                        </h2>
                    </div>
                    <form class="address-book-form" method="POST" action="{{ route('addresses.store') }}">
                        @csrf
                        <div class="float-field">
                            @error('name')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" value="{{old('name')}}" name="name" placeholder="@lang('Full name')">
                            <label>@lang('Full name')</label>
                        </div>
                        <div class="float-field">
                            @error('phone')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" value="{{old('phone')}}" name="phone"
                                   placeholder="@lang('Phone number')">
                            <label>@lang('Phone number')</label>
                        </div>

                        <div class="float-field">
                            @error('type')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <select class="new-address-select" name="type">
                                <option value="">@lang("Select type")</option>
                                <option @if(old("type") == "primary")  selected
                                        @endif value="primary">@lang("Primary")</option>
                                <option @if(old("type") == "secondary")  selected
                                        @endif value="secondary">@lang("Secondary")</option>
                                <option @if(old("type")== "shipping")  selected
                                        @endif value="shipping">@lang("Shipping")</option>
                            </select>
                            <label>@lang('Type')</label>
                        </div>

                        <div class="float-field">
                            @error('country_id')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <select class="new-address-select country-select" name="country_id">
                                <option disabled selected>@lang("Select country")</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                            <label>@lang('Country')</label>
                        </div>
                        <div class="float-field">
                            @error('city_id')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <select class="new-address-select city-select" disabled name="city_id">
                                <option>@lang("Choose country first")</option>
                            </select>
                            <label>@lang('City')</label>
                        </div>

                        <div class="float-field">
                            @error('state')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" name="state" value="{{ old('state') }}" placeholder="@lang('State')">
                            <label>@lang('State')</label>
                        </div>

                        <div class="float-field">
                            @error('street')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" name="street" value="{{ old('street') }}"
                                   placeholder="@lang('Street name')">
                            <label>@lang('Street name')</label>
                        </div>

                        <div class="float-field">
                            @error('zip_code')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" name="zip_code" value="{{ old('zip_code') }}"
                                   placeholder="@lang('Zip code')">
                            <label>@lang('Zip code')</label>
                        </div>

                        <div class="float-field">
                            @error('note')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <input type="text" name="note" value="{{ old('note') }}" placeholder="@lang('Note')">
                            <label>@lang('Note')</label>
                        </div>

                        <div class="new-address-map">
                            @error('map_location')
                            <span class="error-text-alert">
                                {{ $message }}
                            </span>
                            @enderror
                            <div id="new-address-map"></div>
                            <a role="button" class="locationButton">
                                <img src="{{Ecommerce::theme()->asset('images/checkout/location.png')}}">
                            </a>
                            <div class="locationInput-container" style="margin: 10px 0;">
                                <input type="hidden" id="map-lat" name="map_location[lat]">
                                <input type="hidden" id="map-lng" name="map_location[lng]">
                                <input type="text" id="locationInput" name="map_location[address]"
                                       class="locationInput">
                            </div>
                        </div>

                        <div>
                            <button type="submit" class="submit-btn">
                                @lang('Save')
                            </button>
                        </div>

                    </form>
                </div>
                <aside id="column-right" class="col-sm-3 hidden-xs">
                    @include('theme::account.menu')
                </aside>

            </div>
        </div>
    </section>
    <!-- End Page Content -->

@endsection
@push("styles.before")
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
@endpush
@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $("select").select2();
        $(function () {
            $(".country-select").on("change", function () {
                let country = $(this).val();
                let select = $(".city-select");
                select.empty().attr("disabled", "disabled");
                $.ajax({
                    url: route("ajax.countries.country.cities", country),
                    success: function (data) {
                        select.html(data).removeAttr("disabled");
                    }
                });
            });
        });

        function initMap() {
            const myLatLng = {
                lat: 24.774255,
                lng: 46.737586534
            };
            let map = new google.maps.Map(document.getElementById("new-address-map"), {
                center: myLatLng,
                zoom: 17,
            });
            let marker = new google.maps.Marker({
                position: map.getCenter(),
                draggable: true,
                map,
                icon: "{{Ecommerce::theme()->asset('images/pin.png')}}"
            });
            const locationButton = document.getElementsByClassName('locationButton')[0];
            locationButton.addEventListener("click", () => {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(
                        (position) => {
                            const pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude,
                            };
                            marker.setPosition(pos);
                            map.setCenter(pos);
                            updateInput(pos)
                        },
                    );
                }
            });

            google.maps.event.addListener(marker, "dragend", function (e) {
                updateInput(e.latLng)
            });
        }

        function updateInput(latLng) {
            var geocoder = new google.maps.Geocoder();
            $('#map-lat').val(latLng.lat);
            $('#map-lng').val(latLng.lng);
            geocoder.geocode({
                'latLng': latLng
            }, function (results) {
                $('#locationInput').val(results[0].formatted_address);
                document.getElementsByClassName('locationInput')[0].value = results[0].formatted_address;
            });
        }
    </script>
    {{Utilities::MapScriptTag(['callback'=>'initMap'])}}
@endpush
