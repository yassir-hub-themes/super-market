@extends("theme::layouts.master")
@section('page_title',__('Address book'))
@section("content")
    <section class="content-section address-book-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="acc-head">
                        <h2 class="acc-title">
                            @lang('Address book')
                        </h2>
                        <a href="{{ route('addresses.create') }}" class="add-new">
                            @lang('Add new address')
                        </a>
                    </div>
                    <div class="address-list">
                        @foreach($addresses as $address)
                            <div class="adress-item">
                                <div class="adress-head">
                                    <span class="name">
                                        {{ $address->name }}
                                    </span>
                                    <div class="item-tools">
                                        <a href="#!" class="checkmark item-tool default">
                                            <i class="fas fa-check-circle"></i>
                                        </a>
                                        <a href="{{ route('addresses.edit',$address->id) }}" class="edit item-tool">
                                            <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('addresses.delete',$address->id) }}" class="delete item-tool">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <p class="address-text">
                                    {{ $address->state }}
                                </p>
                                <strong class="address-phone">
                                    {{ $address->phone }}
                                </strong>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center">{{$addresses->links()}}</div>
                </div>
                <aside id="column-right" class="col-sm-3 hidden-xs">
                    @include('theme::account.menu')
                </aside>

            </div>
        </div>
    </section>
@endsection
