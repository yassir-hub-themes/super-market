@extends("theme::layouts.master")
@section('page_title',__('Orders'))
@section("content")
    <section class="content-section my-acc-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="acc-head">
                        <h2 class="acc-title">
                            @lang('Orders')
                        </h2>
                    </div>
                    <div class="orders-list">
                        @forelse($orders as $order)
                            <div class="order-item">
                                @include('theme::account.orders.order.head',['order'=>$order])
                                @include('theme::account.orders.order.progress',['order'=>$order])
                                @include('theme::account.orders.order.summary',['order'=>$order])
                                @include('theme::account.orders.order.details-link',['order'=>$order])
                            </div>
                        @empty
                            <p>@lang("No orders")</p>
                        @endforelse
                    </div>
                    {{$orders->links()}}
                </div>
                <aside id="column-right" class="col-sm-3 hidden-xs">
                    @include('theme::account.menu')
                </aside>
            </div>
        </div>
    </section>
@endsection
