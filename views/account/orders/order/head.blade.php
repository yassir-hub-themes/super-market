<?php
?>
<div class="order-head justify-content-between">
    <div class="d-flex">
        <div class="order-no">
            @lang('Order number') <span>{{ $order->id }}</span>
        </div>
        <div class="order-date">
            {{ $order->created_at->format('M d, Y') }}
        </div>
    </div>
    @if($order->isThroughAStatus(\Tasawk\Orders\Models\Order\StatusList::new()))
        <div class="order-no delivery-at">
            @lang('Delivery at'):
            <span>{{$order->delivery_at}}</span>
        </div>
    @endif
</div>
