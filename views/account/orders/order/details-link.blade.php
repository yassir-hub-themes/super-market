<div class="details-link-cont">
    <a href="{{ route('orders.show',$order->id) }}" class="order-details">
        @lang('Order details')
        <i class="fas fa-chevron-{{app()->getLocale() == 'ar'?'left':'right'}}"></i>
    </a>
</div>
