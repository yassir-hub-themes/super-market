<div class="order-summary">
    @php($cart = $order->cart->instance())
    @foreach ($cart->getContent() as $item)
        <div class="summry-item">
            <div class="order-qty">
                x {{ $item->quantity }}
            </div>
            <div class="order-name">
                <a href="{{route("shop.show",[Str::plural($item->associatedModel['type']),$item->associatedModel['id']])}}"
                   class="name">
                    {{ $item->name ?? ''}}
                </a>
            </div>
            <div class="order-price">
                {{ $item->quantity * $item->price }}
            </div>
        </div>
    @endforeach
</div>
