<?php
use \Tasawk\Orders\Models\Order;
use \Tasawk\Orders\Models\Order\StatusList;
?>
<div class="order-progress">
    @if($order->status == StatusList::canceled())
        @foreach([StatusList::new(), StatusList::canceled()] as $status)
            <div class="order-step @if($order->isThroughAStatus(StatusList::canceled())) done @endif">
                <div class="step-icon"><i class="fas fa-check"></i></div>
                <span class="step-name">@lang(Str::headline($status))</span>
            </div>
        @endforeach
    @endif
    @if($order->status == StatusList::rejected())
        @foreach([StatusList::new() ,StatusList::rejected()] as $status)
            <div class="order-step @if($order->isThroughAStatus(StatusList::rejected())) done @endif">
                <div class="step-icon"><i class="fas fa-check"></i></div>
                <span class="step-name">@lang(Str::headline($status))</span>
            </div>
        @endforeach
    @endif
    @if(!($order->status == StatusList::canceled() || $order->status == StatusList::rejected()))
        @foreach([StatusList::new(), StatusList::inProcessing(), StatusList::outForDelivery(), StatusList::delivered(), StatusList::completed()] as $status)
            <div class="order-step @if($order->isThroughAStatus($status)) done @endif">
                <div class="step-icon"><i class="fas fa-check"></i></div>
                <span class="step-name">@lang(Str::headline($status))</span>
            </div>
        @endforeach
    @endif
</div>
