@extends("theme::layouts.master")
@section('page_title',__('Show order'))
@section("content")
    <section class="content-section order-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-9">
                    <div class="acc-head">
                        <h2 class="acc-title">
                            @lang('Order details')
                        </h2>
                        {{--                        <a href="{{ route('orders.reorder',$order->id) }}" class="reorder">--}}
                        {{--                            <i class="fas fa-redo"></i>--}}
                        {{--                            @lang('Reorder')--}}
                        {{--                        </a>--}}
                    </div>
                    <div class="single-order">
                        @include('theme::account.orders.order.head',['order'=>$order])
                        @include('theme::account.orders.order.progress',['order'=>$order])
                        @include('theme::account.orders.order.summary',['order'=>$order,'cart'=>$cart = $order->as_cart])
{{--                        <div class="order-summary">--}}
{{--                            @php($cart = $order->as_cart)--}}
{{--                            @foreach ($cart->getContent() as $item)--}}
{{--                                <div class="summry-item">--}}
{{--                                    <div class="order-name">--}}
{{--                                        <a href="{{route("shop.show",[Str::plural($item['associatedModel']['type']),$item['associatedModel']['id']])}}"--}}
{{--                                           class="name">--}}
{{--                                            {{ $item['name']}}--}}
{{--                                        </a>--}}
{{--                                        <div class="single-qty-price">--}}
{{--                                            <div class="order-qty">--}}
{{--                                                x {{ $item->quantity }}--}}
{{--                                            </div>--}}
{{--                                            <div class="single-price">--}}
{{--                                                {{ $item->price }}--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="order-price">--}}
{{--                                        {{ $item->getPriceSum() }}--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
                        <div class="totals">
                            <div class="total">
                                <span class="title">
                                    @lang('Subtotal')
                                </span>
                                <span class="value">
                                    {{ $cart->getSubTotal() }}
                                </span>
                            </div>
                            @foreach($cart->getConditions() as $condition)
                                <div class="total">
                                    <span class="title">@lang(ucfirst($condition->getType()))</span>
                                    <span class="value">{{$condition->getValue()}}</span>
                                </div>
                            @endforeach
                            <div class="total">
                                <span class="title">
                                    @lang('Total')
                                </span>
                                <span class="value">
                                    {{ $cart->getTotal() }}
                                </span>
                            </div>
                        </div>
                        <div class="payment">
                            <h4 class="title">
                                @lang('Payment method') ( {{ __($order->payment_method) }} )
                            </h4>
                            <div class="method">
                                <?php
                                $payments = json_decode(settings()->group("ecommerce")->get("allowed_payment_methods"), true) ?? [];
                                ?>
                                <img src="{{ upload_storage_url(Ecommerce::settings()->get("$order->payment_method-image")) }}"
                                     class="img-fluid">
                            </div>
                        </div>
                        <div class="location">
                            <h4 class="title">
                                @lang('Delivery address')
                            </h4>
                            <div class="order-map">
                                <div id="order-map"></div>
                                <div class="locationInput-container">
                                    <input disabled type="text" class="locationInput"
                                           value="<?php echo e($order->map_address['address_name']) ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <aside id="column-right" class="col-sm-3 hidden-xs">
                    @include('theme::account.menu')
                </aside>

            </div>
        </div>
    </section>
@endsection
@push('scripts')
    <script>
        function initMap() {
            const myLatLng = {
                lat: parseFloat("<?php echo e($order->map_address['lat']) ?>"),
                lng: parseFloat("<?php echo e($order->map_address['lang']) ?>")
            };
            let map = new google.maps.Map(document.getElementById("order-map"), {
                center: myLatLng,
                zoom: 17,
            });
            let marker = new google.maps.Marker({
                position: map.getCenter(),
                map,
                icon: "{{Ecommerce::theme()->asset('images/pin.png')}}"
            });
        }
    </script>
    {{Utilities::MapScriptTag(['callback'=>'initMap'])}}
@endpush
