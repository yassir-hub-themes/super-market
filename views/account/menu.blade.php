<div class="list-group">
    @if(!Ecommerce::auth()->check())
        <a href="{{ route('login.form') }}"
           class="list-group-item  @if(Route::currentRouteName() == 'login.form') active @endif">@lang('Login')</a>
        <a href="{{ route('register') }}"
           class="list-group-item  @if(Route::currentRouteName() == 'register') active @endif">@lang('Register')</a>
    @endif
    <a href="{{ route('profile.index') }}"
       class="list-group-item  @if(request()->routeIs('profile*')) active @endif">@lang('Account information')</a>
    <a href="{{ route('password.index') }}"
       class="list-group-item @if(request()->routeIs('password*')) active @endif"> @lang('Change password')</a>
    <a href="{{ route('addresses.index') }}"
       class="list-group-item @if(request()->routeIs('addresses*')) active @endif"> @lang('Address book')</a>
    <a href="{{ route('orders.index') }}"
       class="list-group-item @if(request()->routeIs('orders*')) active @endif"> @lang('My orders')</a>
    <a href="{{ route('ecommerce.logout') }}" class="list-group-item">  @lang('Logout')</a>
</div>


