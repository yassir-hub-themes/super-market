@extends("theme::layouts.master")
@section('page_title',__('Change password'))
@section("content")

    <!-- Start Page Content -->
    <section class="content-section my-acc-content">
        <div class="container">
            <div class="row">

                <div id="content" class="col-sm-9">
                    <div class="register-section">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="well">
                                    <h2>تسجيل جديد</h2>
                                    <p><strong>تسجيل جديد</strong></p>
                                    <p>لكي تقوم بإنهاء الطلب قم بإنشاء حساب جديد معنا، فهو يُمكنك من الشراء بصورة أسرع و
                                        متابعة طلبيات الشراء التي تقدمت بها, و مراجعة سجل الطلبيات القديمة واسعتراض الفواتير
                                        وغير ذلك الكثير...</p>
                                    <a href="#" class="btn btn-primary">متابعة</a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="well">
                                    <h2>تسجيل الدخول</h2>
                                    <p><strong>إذا كنت تملك حساب مسبق في الموقع، فتفضل بتسجيل دخولك...</strong></p>
                                    <form action="#" method="post" enctype="multipart/form-data">
                                        <div class="form-margin">
                                            <label class="control-label" for="input-email">البريد الالكتروني </label>
                                            <input type="text" name="email" value="" placeholder="البريد الالكتروني "
                                                   id="input-email" class="form-control">
                                        </div>
                                        <div class="form-margin">
                                            <label class="control-label" for="input-password">كلمة المرور </label>
                                            <input type="password" name="password" value="" placeholder="كلمة المرور "
                                                   id="input-password" class="form-control">
                                            <a class="forget-link" href="#">هل نسيت كلمة المرور؟</a>
                                        </div>
                                        <input type="submit" value="تسجيل دخول " class="btn btn-primary">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <aside id="column-right" class="col-sm-3 hidden-xs">
                    @include('theme::account.menu')
                </aside>
            </div>
        </div>
    </section>
    <!-- End Page Content -->


@endsection
