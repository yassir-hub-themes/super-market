@extends("theme::layouts.master")
@section('page_title',__('My account'))
@section("content")

    <!-- Start Page Content -->
    <section class="content-section my-acc-content">
        <div class="container">
            <div class="row">


                <div class="col-lg-9">
                    <div class="acc-head">
                        <h2 class="acc-title">
                            @lang('My account')
                        </h2>
                    </div>
                    <form class="acc-form" action="{{ route('profile.store') }}" method="POST">
                        @csrf
                        <div class="float-field">
                            @error('name')
                            <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                            @enderror
                            <input type="text" name="name" value="{{ Ecommerce::auth()->user()->name }}"
                                   placeholder="@lang('Full name')">
                            <label>@lang('Full name')</label>
                        </div>
                        <div class="float-field">
                            @error('email')
                            <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                            @enderror
                            <input type="email" name="email" value="{{ Ecommerce::auth()->user()->email }}"
                                   placeholder="@lang('Email')">
                            <label>@lang('Email')</label>
                        </div>
                        <div class="float-field">
                            @include("theme::components.phone_input",['value'=>Ecommerce::auth()->user()->phone])
                            <label>@lang('Phone number')</label>
                        </div>
                        <a href="{{ route('password.index') }}" class="edit-pass-link">
                            @lang('Change password')
                            @if (\App::getLocale() == 'ar')
                                <i class="fas fa-arrow-left"></i>
                            @else
                                <i class="fas fa-arrow-right"></i>
                            @endif
                        </a>
                        <button class="submit-btn">
                            @lang('Save changes')
                        </button>
                    </form>
                </div>
                <aside id="column-right" class="col-sm-3 hidden-xs">
                    @include('theme::account.menu')
                </aside>
            </div>
        </div>
    </section>
    <!-- End Page Content -->


@endsection
