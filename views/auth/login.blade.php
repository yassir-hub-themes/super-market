@extends("theme::layouts.master")
@section('page_title',__('Register'))
@section("content")

    <div class="register-section">
        <div class="container">
            <div class="row">
                <div id="content" class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="well">
                                <h2>@lang('New Customer')</h2>
                                <p><strong>@lang('Register')</strong></p>
                                <p>@lang("By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.")</p>
                                <a href="{{route('register')}}" class="btn btn-primary p-0">@lang("Continue")</a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="well">
                                <h2>@lang('Returning Customer')</h2>
                                <p><strong>@lang('I am a returning customer')</strong></p>
                                @include("theme::components.forms.login")

                                <a href="{{route('forget-password.index')}}" style="color: grey">@lang('Forgotten Password')</a>
                            </div>
                        </div>
                    </div>
                </div>

                <aside id="column-right" class="col-sm-3 hidden-xs">
                    @include("theme::account.menu")
                </aside>
            </div>
        </div>
    </div>

@endsection

