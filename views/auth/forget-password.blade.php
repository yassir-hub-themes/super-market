@extends("theme::layouts.master")
@section('page_title',__('Forget password'))
@section("content")
    <section class="content-section register-content">
        <div class="container">
            <h2 class="page-title">
                @lang('Forget password')
            </h2>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <form class="register-form" action="{{ route('forget-password.check') }}" method="POST">
                        @csrf
                        @if (in_array(Ecommerce::settings()->get("ecommerce-login-by"),["phone-and-password","phone-only"]))
                            @include("theme::components.phone_input")
                            @error('object')
                            <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
                            @enderror
                        @else
                            <div class="form-group email">
                                <label>
                                    @lang('Email')
                                </label>
                                <input type="email" value="{{ old('object') }}" name="object" class="form-control" autocomplete="off">
                                @error('object')
                                <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
                                @enderror
                            </div>
                        @endif
                        <button type="submit" class="submit-btn">
                            @lang('Submit')
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
