@extends("theme::layouts.master")
@section('page_title',__('Confirmation code'))
@section("content")
    <!-- Start Page Content -->
    <section class="content-section register-content">
        <div class="container">
            <h2 class="page-title">
                @lang('Confirmation code')
            </h2>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <form class="register-form" action="{{ route('code-confirm.check') }}" method="POST">
                        @csrf
                        <div class="form-group name required">
                            <label>
                                @lang('Code')
                            </label>
                            <input type="text" value="{{ old('code') }}" name="code" class="form-control">
                            @error('code')
                            <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
                            @enderror
                        </div>
                        <button type="submit" class="submit-btn">
                            @lang('Submit')
                        </button>
                    </form>
                </div>
            </div>
    </section>
    <!-- End Page Content -->
@endsection
