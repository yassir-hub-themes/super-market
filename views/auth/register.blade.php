@extends("theme::layouts.master")
@section('page_title',__('Register'))
@section("content")

    <div class="register-two-section">
        <div class="container">
            <div class="row">
                <div id="content" class="col-sm-9">
                    <h1>@lang("Account")</h1>
                    <p>@lang('If you already have an account with us, please login at') <a class="login-link-page"
                                                                                           href="{{route('login.form')}}">@lang('the login page.')</a>
                    </p>

                    <form class="register-form form-horizontal" action="{{ route('register.check') }}" method="POST">
                        @csrf
                        <fieldset id="account">
                            <legend>@lang('Your personal details')</legend>
                            <div class="form-group">
                                @include("theme::components.phone_input",['label'=>'Phone'])
                            </div>
                            <div class="form-margin required">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-lastname">   @lang('Full name')</label>
                                    <div class="col-sm-10">
                                        <input type="text" value="{{ old('name') }}" name="name" class="form-control"
                                               autocomplete="new-password">
                                        @error('name')
                                        <span class="error-text-alert ml-4">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-margin required">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-email">@lang('Email')</label>
                                    <div class="col-sm-10">
                                        <input type="email" value="{{ old('email') }}" name="email" class="form-control"
                                               autocomplete="off" id="input-email">
                                        @error('email')
                                        <span class="error-text-alert ml-4">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="form-margin required">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="input-password">@lang('Password')</label>
                                    <div class="col-sm-10">
                                        <input id="password" type="password" name="password" class="form-control"
                                               autocomplete="new-password">
                                        @error('password')
                                        <span class="error-text-alert ml-4">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        @php($terms  = \Tasawk\Cms\Model\Cms::find(Ecommerce::settings()->get('ecommerce-page-terms-of-service')))
                        <div class="buttons" style="display: flex;justify-content: {{$terms?'space-between':'end'}}; ">
                            @if($terms)
                                <div style="margin: 0px 35px">
                                    <input type="checkbox" name="terms" id="terms-check"
                                           @if(old('terms')) checked @endif>
                                    @lang('I have read and agree to the')
                                    <a href="{{route("page.show",$terms?->page_slug)}}"
                                       target="_blank"
                                       class="agree"
                                    >

                                        <b>@lang('Terms and conditions')</b>
                                    </a>
                                    <div>
                                        @error('terms')
                                        <span class="error-text-alert ml-4">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            @endif

                            <div>
                                <input type="submit" value="@lang('Submit')" class="btn btn-primary">
                            </div>
                        </div>

                    </form>
                </div>
                <aside id="column-right" class="col-sm-3 hidden-xs">
                    @include("theme::account.menu")
                </aside>
            </div>
        </div>
    </div>

@endsection

