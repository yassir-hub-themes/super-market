<script src="{{Ecommerce::theme()->asset("js/jquery-2.1.1.min.js")}}"></script>
<script src="{{Ecommerce::theme()->asset("js/autoComplete.min.js")}}"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="{{Ecommerce::theme()->asset("js/bootstrap.min.js")}}"></script>
<script src="{{Ecommerce::theme()->asset("js/bootstrap-select.min.js")}}"></script>
<script src="{{Ecommerce::theme()->asset("js/swiper.min.js")}}"></script>
<script src="{{Ecommerce::theme()->asset("js/wow.min.js")}}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src='//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js'></script>
@if(app()->getLocale() == 'ar')
    <script src="{{Ecommerce::theme()->asset('js/lang/ar.js')}}"></script>
@endif
<script src="{{Ecommerce::theme()->asset("js/script.js?v=1.1.1")}}"></script>

<script>
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': _token}
    });
    toastr.options = {
        closeButton: true,
        progressBar: true,
        positionClass: "toast-bottom-right",
        preventDuplicates: true,
        rtl: $('html').attr('dir') === 'rtl'
    }

    function notify(title, text, type = 'success') {
        toastr[type](text, title);
    }

    @if(session('success'))
    notify("@lang('Success')", "{{ session('success') }}");
    @endif
    @if(session('error'))
    notify("@lang('Error')", "{{ session('error') }}", 'error');
    @endif
</script>
<script>
    function updateCartCount(count) {
        $(".cart_items_count").html(count);
    }

    function updateCartPopUp() {
        $.ajax({
            url: route("ajax.cart_popup"),
            success: function (data) {
                $(".cart-popup-container").html(data);
            }
        })
    }

    function cartUpdated(cart) {
        @if(Route::currentRouteName() == "checkout" )
        getCartTotalSummation()
        @endif
        updateCartCount(cart.quantity);
        updateCartPopUp()
        notify("@lang('Done')", "@lang('Cart updated')");
    }

    function addToCart(item, quantity) {
        $.ajax({
            url: route("add-to-cart", [item, quantity]),
            type: "POST",
            success: function (cart) {
                cartUpdated(cart);
            },
            error: function (dd) {
                location.assign(dd.responseJSON['redirect']);
            }
        })
    }

    function applyDeliveryServiceByArea(id, type = 'city') {
        $.ajax({
            url: route('cart.apply_delivery_service', [type, id]),
            type: "POST",
            beforeSend: function () {
                replaceContentWith("#cart-total-summation-container", $("#spinner").html());
            },
            success: function (data) {
                getCartTotalSummation();
            }
        })
    }

    function updateCartItemQuantity(cartId, quantity) {
        $.ajax({
            url: route("update-cart-item-quantity", [cartId, quantity]),
            type: "POST",
            success: function (cart) {
                replaceContentWith("#cart-total-summation-container", $("#spinner").html());
                cartUpdated(cart);
            },
            error: function (dd) {
                location.assign(dd.responseJSON['redirect']);
            }
        })
    }

    function getCartTotalSummation() {
        $.ajax({
            url: route("cart.get-total-summation"),
            success: function (data) {
                $("#cart-total-summation-container").html(data);
            }
        })
    }

    function replaceContentWith(selector, text) {
        $(selector).children().remove();
        $(selector).html(text);
    }

    function changeIconState(selector, status = "loading") {
        let statusHtml = {
            loading: `<i class="fas fa-spinner"></i>`,
            success: `<i class="fas fa-check"></i>`,
            danger: `<i class="fas fa-times"></i>`,
            none: ""
        }
        $(selector).html(statusHtml[status]);
    }

    $(function () {
        // $('#button-cart').on("click", function () {
        //     let input = $(this).parents(".quantity_cart").find("#input-quantity");
        //     let quantity = input.val()
        //     let item = input.data("item");
        //     let cartId = input.data('cart-id');
        //     if (cartId) {
        //         updateCartItemQuantity(cartId, quantity)
        //     } else {
        //         addToCart(item, quantity);
        //     }
        // });

        $('.qty-control').not(".disabled-actions").on("click", function () {
            let input = $(this).parents(".item-qty").find("input.qty-input");
            let quantity = input.val()
            let item = input.data("item");
            let cartId = input.data('cart-id');
            if (!input.data('has-ajax')) {
                return;
            }
            if (quantity <= 0) {
                input.parents(".cart-item").remove();
            }
            if (cartId) {
                updateCartItemQuantity(cartId, quantity)
            } else {
                addToCart(item, quantity);
            }
        });
        $(".item-qty input.qty-input").not(".disabled-actions").on("change", function () {
            let input = $(this);
            let quantity = input.val()
            let item = input.data("item");
            let cartId = input.data('cart-id');
            if (quantity <= 0) {
                input.parents(".cart-item").remove();
            }
            if (cartId) {
                updateCartItemQuantity(cartId, quantity)
            } else {
                addToCart(item, quantity);
            }
        });
        $(document).on("click", "a.item-delete , .delete-item-cart-popup", function () {
            let that = $(this);
            let item = that.data("item");
            replaceContentWith("#cart-total-summation-container", $("#spinner").html());
            $.ajax({
                url: route("remove-item-from-cart", {item}),
                type: "DELETE",
                success: function (cart) {
                    @if(Route::currentRouteName() == "checkout" )
                    if (cart.length === 0) {
                        location.href = '/'
                        // replaceContentWith(".cart-content", $("#empty-cart").html());
                    }
                    getCartTotalSummation();
                    that.parents(".cart-item").remove();
                    @endif
                    if (that.hasClass('delete-item-cart-popup')) {
                        updateCartPopUp();
                    }
                    updateCartCount(cart.length);
                    notify("@lang('Success')", "@lang('Item removed from cart')");

                }
            })
        })
        $(".discount-code-input").on("change", function () {
            let that = $(this);
            let coupon = that.val();
            changeIconState(that.parent().find(".code-btn-input"));
            replaceContentWith("#cart-total-summation-container", $("#spinner").html());
            $.ajax({
                url: route("cart.apply-coupon", {coupon}),
                type: "POST",
                success: function (data) {
                    if (data.status === 200) {
                        if (coupon === "") {
                            changeIconState(that.parent().find(".code-btn-input"), "none");
                        } else {
                            changeIconState(that.parent().find(".code-btn-input"), "success");
                            that.parent().find("input").addClass('active');
                        }
                        replaceContentWith("#cart-total-summation-container", $("#spinner").html());
                        getCartTotalSummation();
                        notify("@lang('Success')", data.message);

                    } else {
                        changeIconState(that.parent().find(".code-btn-input"), "danger");
                        that.parent().find("input").removeClass('active');
                        notify("@lang('Error')", data.message, "error");
                        getCartTotalSummation();


                    }

                },
            })
        })
        $(".voucher-code-input").on("change", function () {
            let that = $(this);
            let coupon = that.val();
            changeIconState(that.parent().find(".code-btn-input"));
            $.ajax({
                url: route("cart.apply-voucher", {coupon}),
                type: "POST",
                success: function (data) {
                    if (data.status === 200) {
                        if (coupon === "") {
                            changeIconState(that.parent().find(".code-btn-input"), "none");
                        } else {
                            changeIconState(that.parent().find(".code-btn-input"), "success");
                            that.parent().find("input").addClass('active');
                        }
                        replaceContentWith("#cart-total-summation-container", $("#spinner").html());
                        getCartTotalSummation();
                        notify("@lang('Success')", data.message);
                    } else {
                        changeIconState(that.parent().find(".code-btn-input"), "danger");
                        notify("@lang('Error')", data.message, "error");

                    }

                },
            })

        });

    });
</script>
@stack("scripts")

{!!Ecommerce::settings()->get('seo-footer-scripts')!!}
