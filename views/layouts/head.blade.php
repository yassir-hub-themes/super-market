<title>@yield('page_title')@if(Ecommerce::settings()->get("ecommerce-site-name"))
        - {{Ecommerce::settings()->get("ecommerce-site-name")}} @endif</title>
@if(Ecommerce::settings()->get('seo-head-description'))
    <meta name="description" content="{{Ecommerce::settings()->get('seo-head-description')}}">
@endif
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no"/>
<meta name="csrf-token" content="{{ csrf_token() }}"/>
<link rel='shortcut icon' type='img/png' href='{{Ecommerce::theme()->asset('shop/images/fav.png')}}'/>
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("css/all.min.css")}}"/>
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("css/bootstrap.min.css")}}"/>
@if(app()->getLocale() == "ar")
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset("css/bootstrap-rtl.min.css")}}"/>
@endif
@stack("styles.before")
<link rel="stylesheet" href="{{Ecommerce::theme()->asset('css/bootstrap-select.min.css')}}">
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("css/fonts.css")}}">
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("css/animate.css")}}">
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("css/autoComplete.css")}}">
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("css/swiper.min.css")}}">
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("css/style.css")}}">
<link rel="stylesheet" href="{{Ecommerce::theme()->asset("css/custom.css")}}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/css/intlTelInput.css"
      integrity="sha512-gxWow8Mo6q6pLa1XH/CcH8JyiSDEtiwJV78E+D+QP0EVasFs8wKXq16G8CLD4CJ2SnonHr4Lm/yY2fSI2+cbmw=="
      crossorigin="anonymous" referrerpolicy="no-referrer"/>
@stack("styles")
