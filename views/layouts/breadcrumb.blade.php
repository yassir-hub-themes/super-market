@hasSection('breadcrumb')
    @yield('breadcrumb')
@else
    @if(Route::currentRouteName() != "home")
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/">@lang("Home")</a></li>
                @foreach($breadcrumbs??[] as $breadcrumb)
                    <li><a href="{{$breadcrumb['route']}}">@lang($breadcrumb['title'])</a></li>
                @endforeach
            </ul>

        </div>
    @endif
@endif
