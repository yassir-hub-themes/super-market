@php
    use Tasawk\Items\Model\Items;
    $about_page_id =Ecommerce::settings()->get('ecommerce-page-about');
    $term_of_services_page_id = Ecommerce::settings()->get('ecommerce-page-terms-of-service');
    $privacy_policy_page_id = Ecommerce::settings()->get('ecommerce-page-privacy-policy');
    $pages_ids = array_unique([$about_page_id,$term_of_services_page_id,$privacy_policy_page_id]);
    $pages = \Tasawk\Cms\Model\Cms::find($pages_ids)->pluck("page_slug","id");
    $services_count = Items::where("type", 'service')->count();
@endphp
        <!DOCTYPE HTML>
<html lang="{{app()->getLocale()}}" dir="{{app()->getLocale() == 'ar' ? 'rtl':'ltr' }}">
<head>
    @include("theme::layouts.head")
    @routes
</head>
<script>var _token = '{{csrf_token()}}'</script>
{!!Ecommerce::settings()->get('seo-head-scripts')!!}
<body>
<div class="outer-wrapper">
    @php
        $categories = \Tasawk\Items\Models\Category::enabled()->parent()->get();
    @endphp
    @include("theme::layouts.side_nav")
    <div id="main">
        @include("theme::layouts.main_nav")
        @include("theme::layouts.breadcrumb")
        @yield("content")
        <div class="overlay-box"></div>
        <div class="overlay-box2"></div>
        @include("theme::layouts.footer")
    </div>
</div>
@include("theme::components.modals.login_modal")
@stack("modals")
@include("theme::layouts.scripts")
</body>
</html>


