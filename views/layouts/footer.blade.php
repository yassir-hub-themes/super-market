<footer>
    <div class="container-fluid">
        <div class="first-footer">
            <div class="footer-col">
                <figure class="footer-figure">
                    <img
                        @if($img_url = Ecommerce::settings()->get("ecommerce_site_logo"))
                        @elseif($img_url = settings()->group('default')->get('logo'))
                        @else
                        @php($img_url =AdminBase::theTheme()->asset('app-assets/images/svg/logo-color.svg'))
                        @endif
                        src="{{upload_storage_url($img_url)}}"
                        alt="@lang('logo')"
                        title="@lang('logo')"
                        class="img-responsive"
                    >
                </figure>
                <div class="social">
                    @foreach(array_filter(settings()->json("social_links"))??[] as $key => $social )
                        <a href="{{$social['link']}}" class="social-link">
                            <i class="{{$social['icon']}}"></i>
                        </a>
                    @endforeach
                </div>
                <figure class="pay-figure">
                    <img
                        src="{{Ecommerce::theme()->asset('images/pay-img.png')}}"
                        class="img-responsive pay-img">
                </figure>
                <figure class="symbol-figure">
                    @if($url = settings()->group("ecommerce")->get("maroof_logo"))
                        <div class="maroof">
                            <a href="{{ settings('contact-maroof-link') }}" class="maroof">
                                <img src="{{upload_storage_url($url)}}" class="img-responsive">
                            </a>
                        </div>
                    @endif

                </figure>
            </div>
            <div class="footer-col">
                <div class="nav-foot-cont">
                    <h2 class="nav-foot-header foot-header" data-toggle="collapse"
                        data-target="#footer-list1">@lang('contact us')</h2>
                    <ul class="nav-foot list-unstyled collapse" id="footer-list1">
                        @if(settings()->get('contact-address'))
                            <li class="nav-foot-li">
                                <span class="nav-foot-span">@lang('Address'): </span>
                                <a href="#" class="nav-foot-link">{{settings()->get('contact-address')}}</a>
                            </li>
                        @endif
                        @if($contact_us_phone = settings()->group("ecommerce")->get('ecommerce-site-phone'))
                            <li class="nav-foot-li">
                                <span class="nav-foot-span">@lang('Phone'):</span>
                                <a href="tel:{{$contact_us_phone}}" class="nav-foot-link">{{$contact_us_phone}}</a>
                            </li>
                        @endif
                        @if($contact_us_email = settings()->group("ecommerce")->get('ecommerce-site-email'))
                            <li class="nav-foot-li">
                                <span class="nav-foot-span">@lang('Email'):</span>
                                <a href="mailto:{{$contact_us_email}}"
                                   class="nav-foot-link">{{$contact_us_email}}</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="footer-col">
                <div class="nav-foot-cont">
                    <h2 class="nav-foot-header foot-header" data-toggle="collapse"
                        data-target="#footer-list2">@lang('Information')</h2>

                    <ul id="footer-list2" class="nav-foot list-unstyled collapse">
                        @if(Arr::exists($pages,$about_page_id) && $slug = Arr::get($pages,$about_page_id))
                            <li class="nav-foot-li">
                                <a href="{{route("page.show",$slug)}}" class="nav-foot-link">
                                    @lang('About us')
                                </a>
                            </li>
                        @endif
                        @if($banks = \Tasawk\Banks\Model\Banking::count())
                            <li class="nav-foot-li">
                                <a href="{{ route('bank-accounts') }}" class="nav-foot-link">
                                    @lang('Bank accounts')
                                </a>
                            </li>
                        @endif
                        @if(Arr::exists($pages,$term_of_services_page_id) && $slug = Arr::get($pages,$term_of_services_page_id))
                            <li class="nav-foot-li">
                                <a href="{{route("page.show",$slug)}}" class="nav-foot-link">
                                    @lang('Terms and conditions')
                                </a>
                            </li>
                        @endif

                        @if(Arr::exists($pages,$privacy_policy_page_id) && $slug = Arr::get($pages,$privacy_policy_page_id))
                            <li class="nav-foot-li">
                                <a href="{{route("page.show",$slug)}}" class="nav-foot-link">
                                    @lang('Privacy policy')
                                </a>
                            </li>
                        @endif
                        <li class="nav-foot-li">
                            <a href="{{ route('contact-us') }}" class="nav-foot-link">
                                @lang('Contact us')
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
            {{--            <div class="footer-col">--}}
            {{--                <div class="nav-foot-cont">--}}
            {{--                    <h2 class="nav-foot-header foot-header" data-toggle="collapse"--}}
            {{--                        data-target="#footer-list3">@lang('extra')</h2>--}}
            {{--                    <ul class="nav-foot list-unstyled collapse" id="footer-list3">--}}
            {{--                        <li class="nav-foot-li">--}}
            {{--                            <a href="#" class="nav-foot-link">@lang('Return orders')</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-foot-li">--}}
            {{--                            <a href="#" class="nav-foot-link">@lang('companies')</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-foot-li">--}}
            {{--                            <a href="#" class="nav-foot-link">@lang('Gift vouchers')</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-foot-li">--}}
            {{--                            <a href="#" class="nav-foot-link">@lang('commission system')</a>--}}
            {{--                        </li>--}}
            {{--                        <li class="nav-foot-li">--}}
            {{--                            <a href="#" class="nav-foot-link">@lang('Special Offers')</a>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            <div class="footer-col">
                <div class="nav-foot-cont">
                    <h2 class="nav-foot-header foot-header" data-toggle="collapse"
                        data-target="#footer-list4">@lang('My account')</h2>
                    <ul class="nav-foot list-unstyled collapse" id="footer-list4">
                        <li class="nav-foot-li">
                            <a href="{{route('profile.index')}}" class="nav-foot-link">@lang('My account')</a>
                        </li>
                        <li class="nav-foot-li">
                            <a href="{{route('orders.index')}}" class="nav-foot-link">@lang('My orders')</a>
                        </li>
                        <li class="nav-foot-li">
                            <a href="{{route('addresses.index')}}" class="nav-foot-link">@lang('Address book')</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="copy-right">
            @php($date = date("Y"))
            <span class="right-text">
                 @lang('All rights reserved for YassirHub © :DATE',['DATE' => $date])
            </span>
            <span class="design-text">
                @lang('Designed & Developed by')
                <a href="https://yassirhub.com">
                    <img style="max-width: 30px;" src="{{AdminBase::theTheme()->asset('app-assets/images/svg/logo-color.svg')}}" alt="" class="footer-made">
                </a>
            </span>
        </div>
    </div>
</footer>
