<header>
    @php($banner_features = settings()->group("theme_super_market")->json("features"))
    @if($banner_features)
        <div class="upper-header">
            <div class="container-fluid ">
                <div class="upper-cont">
                    <div class="upper-info d-flex align-items-center">
                        @foreach(settings()->group("theme_super_market")->json("features") as $key=>$feature )
                            <div class="upper-text">
                                <i class="{{$feature['icon']??''}}"></i>
                                <span class=" d-block"
                                      style="{{app()->getLocale() == 'ar'?'margin-right: 4px':'margin-left: 4px'}}">{{$feature[app()->getLocale()]['title']??''}}</span>
                            </div>
                        @endforeach
                    </div>

                    <div class="upper-lang">
                        @php($convertLang = (\App::getLocale() == "en") ? "ar" : "en")
                        <a href="{{ LaravelLocalization::getLocalizedURL($convertLang, null, [], true) }}"
                           class="lang">
                            {{$convertLang == 'en'?'English':"العربية"}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="top-header navbar-sticky">
        <div class="container-fluid position-relative">
            <div class="first-header">
                <div class="icon-logo">
                    <div class="show-icons">
                        <a class="menu-bars fixall" id="menu-id" href="#!" onclick="openNav()">
                            <i class="fas fa-bars"></i>
                        </a>
                    </div>
                    <figure class="img-logo">
                        <a href="{{request()->root()}}">
                            <img src="{{Ecommerce::logo()}}" class="img-responsive" alt="@lang('logo')"
                                 title="@lang('logo')">
                        </a>
                    </figure>
                </div>
                <div class="search-section">
                    <form class="search-form" action="{{route('shop.search')}}">
                        <input class="search-input"
                               id="autoComplete"
                               spellcheck=false autocorrect="off" autocomplete="off" autocapitalize="off"
                               type="search" name="term" value="{{request()->get('term')}}" placeholder="@lang('What are you looking for?')">
                        <button class="search-button">
                            <span class="fa fa-search"></span>
                        </button>
                    </form>
                </div>
                <div class="icons-cont">
                    <div class="register-section">
                        @if(!Ecommerce::auth()->check())
                            <div class="user-log">
                                <div class="dropdown">
                                    <a href="#" class="user-ancor" data-toggle="modal"
                                       data-target="#login-modal">
                                        <i class="fas fa-user"></i>
                                        <span class="hide-txt">@lang('Login / New registration')</span>
                                    </a>
                                    <div class="dropdown-content">
                                        <a class="cat-drop" href="#"
                                           onclick="location.assign('{{route('login.form')}}')">@lang("Login")</a>
                                        <a class="cat-drop" href="#"
                                           onclick="location.assign('{{route('register')}}')">@lang('New registration')</a>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="user-logged-in">
                                <div class="logged-drop">
                                    <a href="#" class="user-ancor">
                                        <i class="fas fa-user"></i>
                                        @lang('My account')
                                        <i class="fas fa-angle-down fa-xs"></i>
                                    </a>
                                    <ul class="logged-content">
                                        <li><a href="{{ route('profile.index') }}"> @lang('My account')</a></li>
                                        <li><a href="{{ route('addresses.index') }}">@lang('Address book')</a></li>
                                        <li><a href="{{ route('orders.index') }}">@lang("Orders")</a></li>
                                        <li><a href="{{ route('ecommerce.logout') }}">@lang('Logout')</a></li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                        @if(!$banner_features)
                            @php($convertLang = (\App::getLocale() == "en") ? "ar" : "en")
                            <div class="upper-lang">
                                <a href="{{ LaravelLocalization::getLocalizedURL($convertLang, null, [], true) }}"
                                   class="lang">
                                    {{$convertLang == 'en'?'English':"العربية"}}
                                </a>
                            </div>
                        @endif
                        <div class="add-to cart">
                            <button class="add-btn" data-toggle="dropdown" class="cartbtn" aria-expanded="false">
                                <span class="fas fa-shopping-cart icon-ancor"></span>
                                <span class="items-count cart_items_count">{{app('cart')->getContent()->count()}}</span>
                            </button>
                            <ul class="dropdown-menu pull-right cart-popup-container"
                                style="{{!Utilities::isRtl()? 'right: 0;left: auto;' : 'right: auto;left: 0;'}}">
                                @include("theme::components.cart_popup")
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="menu-nav">
        <ul class="big-menu list-unstyled">
            <li class="menu-list">
                <a href="{{route('shop.offers')}}" class="menu-link">
                    @lang('Offers and discounts')
                </a>
            </li>
            <li class="menu-list">
                <a href="{{route('shop.recently_arrived')}}" class="menu-link">
                    @lang('recently arrived')
                </a>
            </li>
            @foreach($categories as $category)
                <li class="menu-list @if($category->children->count()) megaenu-item @endif">
                    <a href="{{route('shop.category.products',$category->id)}}"
                       class="menu-link">{{$category->title}}</a>
                    @includeWhen($category->children()->count(),"theme::components.category",['children'=>$category->children])
                </li>
            @endforeach
        </ul>
    </nav>
</header>
