<div id="mySidenav" class="sidenav">
    <ul class="accordion fixall" id="accordionExample">
        <li><a href="{{request()->root()}}">@lang('Home')</a></li>
        <li><a href="{{route('shop.offers')}}">@lang('Offers and discounts')</a></li>
        <li><a href="{{route('shop.recently_arrived')}}">@lang('recently arrived')</a></li>
        <li><a href="#" class="custom-link">@lang('Product Categories')</a></li>
        @foreach($categories as $category)
            <li>
                <div class="drop_">
                    <a class="drop-link" href="{{route('shop.category.products',$category->id)}}">
                        {{$category->title}}
                        @if($category->children->count())
                            <span role="button"
                                  data-toggle="collapse"
                                  data-target="#category_{{$category->id}}">
                            <i class="fas fa-chevron-down"></i>
                        </span>
                        @endif
                    </a>
                    @if($category->children->count())
                        <ul id="category_{{$category->id}}" class="collapse drop-list fixall">
                            @include("theme::components.sidebar_categories")
                        </ul>
                    @endif
                </div>
            </li>
        @endforeach
        <li>
            <div class="lang_menu">
                <h3 class="list-h">@lang("Language")</h3>
                @php($convertLang = (\App::getLocale() == "en") ? "ar" : "en")
                <a href="{{ LaravelLocalization::getLocalizedURL($convertLang, null, [], true) }}"
                   class="lang">
                    {{$convertLang == 'en'?'English':"العربية"}}
                </a>
            </div>
        </li>
    </ul>
</div>
