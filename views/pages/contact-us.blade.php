@extends("theme::layouts.master")
@section('page_title',__('Contact us'))
@section("content")
    <section class="content-section contact-content">
        <div class="container">
            <div class="contact-cont">
                <h2 class="page-title">
                    @lang('Contact us')
                </h2>
                @if(!is_null(settings('contact-map-lat')) || !is_null(settings('contact-map-lng')))
                    <div id="contact-map" style="width: 100%;height: 350px"></div>
                @endif
                <div class="contant-container">
                    <div class="row">
                        <div class="col-lg-6">
                            <h3 class="contact-title">
                                @lang('Get In Touch')
                            </h3>
                            <form class="contact-form" action="{{ route('contact-us.store') }}" method="POST">
                                @csrf
                                <div class="float-field">
                                    @error('name')
                                    <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                                    @enderror
                                    <input type="text" value="{{ old('name') }}" name="name"
                                           placeholder="@lang('Username')">
                                    <label>@lang('Username')</label>
                                </div>
                                <div class="float-field">
                                    @error('email')
                                    <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                                    @enderror
                                    <input type="email" value="{{ old('email') }}" name="email"
                                           placeholder="@lang('Email')">
                                    <label>@lang('Email')</label>
                                </div>
                                <div class="float-field">
                                    @error('phone')
                                    <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                                    @enderror
                                    <input type="phone" value="{{ old('phone') }}" name="phone"
                                           placeholder="@lang('Phone')">
                                    <label>@lang('Phone')</label>
                                </div>
                                <div class="float-field">
                                    @error('subject')
                                    <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                                    @enderror
                                    <input type="text" value="{{ old('subject') }}" name="subject"
                                           placeholder="@lang('Subject')">
                                    <label>@lang('Subject')</label>
                                </div>
                                @if(count($types))
                                    <div class="float-field">
                                        @error('contact_type_id')
                                        <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                                        @enderror
                                        <select name="contact_type_id" class="new-address-select">
                                            <option disabled selected>@lang('Choose')</option>
                                            @foreach ($types as $type)
                                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                        </select>
                                        <label>@lang('Contact type')</label>
                                    </div>
                                @endif
                                <div class="float-field">
                                    @error('message')
                                    <span class="error-text-alert">
                                  {{ $message }}
                                </span>
                                    @enderror
                                    <textarea name="message" rows="7" style="padding: 25px"
                                              placeholder="@lang('Message')">{{ old('message') }}</textarea>
                                    <label>@lang('Message')</label>
                                </div>
                                <button type="submit" class="submit-btn">
                                    @lang('Send')
                                </button>
                            </form>
                        </div>
                        <div class="col-lg-6">
                            <h3 class="contact-title">
                                @lang('Contact Information')
                            </h3>
                            <div class="contact-list">
                                @if($setting = settings("contact-address"))
                                    <div class="contact-item">
                                        <i class="item-icon fas fa-home" title="@lang("Our location")"></i>
                                        <span class="item-name">{{$setting}}</span>
                                    </div>
                                @endif
                                @if($setting = settings("contact-phone"))
                                    <div class="contact-item">
                                        <i class="item-icon fas fa-phone" title="@lang("Phone")"></i>
                                        <span class="item-name">{{$setting}}</span>
                                    </div>
                                @endif
                                @if($setting = settings("contact-mobile"))
                                    <div class="contact-item">
                                        <i class="item-icon fas fa-phone" title="@lang("Phone")"></i>
                                        <span class="item-name">{{$setting}}</span>
                                    </div>
                                @endif
                                @if($setting = settings("contact-whatsapp"))
                                    <div class="contact-item">
                                        <i class="item-icon fas fa-mail-bulk" title="@lang("Whatsapp phone")"></i>
                                        <span class="item-name">{{$setting}}</span>
                                    </div>
                                @endif
                                @if($setting = settings("contact-fax"))
                                    <div class="contact-item">
                                        <i class="item-icon fas fa-fax" title="@lang("Fax phone")"></i>
                                        <span class="item-name">{{$setting}}</span>
                                    </div>
                                @endif
                                @if($setting = settings("contact-main-email"))
                                    <div class="contact-item">
                                        <i class="item-icon fas fa-envelope" title="@lang("Main Email")"></i>
                                        <span class="item-name">{{$setting}}</span>
                                    </div>
                                @endif
                                @if($setting = settings("contact-contact-us-email"))
                                    <div class="contact-item">
                                        <i class="item-icon fas fa-envelope" title="@lang("Email")"></i>
                                        <span class="item-name">{{$setting}}</span>
                                    </div>
                                @endif
                                @if($setting = settings('commercial_registration_no'))
                                    <div class="contact-item">
                                        <i class="item-icon fas fa-balance-scale-left"
                                           title="@lang("Commercial registration no")"></i>
                                        <span class="item-name">{{$setting}}</span>
                                    </div>
                                @endif
                                @if($setting = settings("tax_identification_number"))
                                    <div class="contact-item">
                                        <i class="item-icon fas fa-file-invoice-dollar"
                                           title="@lang("Tax identification number")"></i>
                                        <span class="item-name">{{$setting}}</span>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push("styles")
    <style>
        .float-field input, .float-field textarea {
            -webkit-text-fill-color: #bbb !important;
            min-height: 45px;
            border: none;
            border-radius: 22.5px;
            width: 100%;
            background-color: #f7f7f7;
            padding: 0 30px;
            -webkit-box-shadow: none;
            box-shadow: none;
            -webkit-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }

        .float-field textarea:focus {
            background-color: #f2f2f2;
            outline: none;
        }
    </style>
@endpush
@push('scripts')
    @if(!is_null(settings('contact-map-lat')) || !is_null(settings('contact-map-lng')))
        <script>
            function initMap() {
                const myLatLng = {
                    lat: parseFloat("{{settings('contact-map-lat')}}"),
                    lng: parseFloat("{{settings('contact-map-lng')}}")
                };
                let map = new google.maps.Map(document.getElementById("contact-map"), {
                    center: myLatLng,
                    zoom: 17,
                });
                let marker = new google.maps.Marker({
                    position: map.getCenter(),
                    map,
                    icon: "{{Ecommerce::theme()->asset('images/pin.png')}}"
                });
            }
        </script>
        {{Utilities::MapScriptTag(['callback'=>'initMap'])}}
    @endif
@endpush
