@extends("theme::layouts.master")
@section('page_title',$page->page_title)
@section("content")

    <!-- Start Page Content -->
    <section class="content-section contact-content small-content-height">
        <div class="container">
            <div class="page-title">
                {{$page->page_title}}
            </div>
            {!! $page->content['text']!!}
        </div>
    </section>
    <!-- End Page Content -->

@endsection
