<fieldset class="features">
    <div class="repeatable">
        <input type="hidden" name="features[]" value="">
        @foreach(settings()->group("theme_super_market")->json("features") as $key=>$feature )
            <div class="feature">
                <div class="row d-flex align-items-center">
                    <div class="col-md-10">
                        <div class="tabbable">
                            <ul class="nav nav-tabs">
                                @foreach (getAllowedLang() as $lang => $locale)
                                    <li class="nav-item">
                                        <a style="font-family: system-ui"
                                           class="{{ $loop->iteration == 1 ? 'active' : '' }} nav-link"
                                           href="#tab-{{$lang}}-{{$key}}" data-toggle="tab">
                                            {{$locale['native']}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="tab-content">
                            @foreach (getAllowedLang() as $k => $locale)
                                <div class="tab-pane {{ $loop->first ? 'active' : '' }}"
                                     id="tab-{{$k}}-{{$key}}">
                                    <div class="nav-tabs">
                                        <div class="form-group row">
                                            <label class="control-label col-lg-12">@lang("Title") </label>
                                            <div class="col-lg-12">
                                                <input class="form-control"
                                                       name="features[{{$loop->parent->index}}][{{$k}}][title]"
                                                       placeholder="{{ __("Title") }}"
                                                       value="{{old("features.$key.title",$feature[$k]['title']??'') }}"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-12">@lang("Icon") </label>
                            <select name="features[{{$loop->index}}][icon]" class="form-control select-icon"
                                    id="social_links_icon_{{$loop->index}}">
                                <option value="">@lang("Choose icon")</option>
                                @foreach(Tasawk\Cms\Libs\FontawesomeIcons::get() as $icon=>$title)
                                    <option data-icon="{{$icon}}" value="{{$icon}}"
                                            @if($icon ==Arr::get($feature,'icon')) selected @endif
                                    >
                                        {{$title}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <button type="button" class="btn btn-danger span-2 delete">
                            @lang('Remove')
                        </button>
                    </div>
                </div>

            </div>
        @endforeach

    </div>
    <div class="form-group" style="text-align:center;">
        <input type="button" value="@lang('Add feature')" class="btn btn-primary add" align="center">
    </div>
</fieldset>
@push("scripts")
    @include("theme::settings._top_banner_features_template")
    <script>
        $(function () {
            let iterator = $(".feature").length;
            iterator += 1;
            $(".features .repeatable").repeatable({
                addTrigger: ".features .add",
                deleteTrigger: ".feature .delete",
                template: "#features-template",
                itemContainer: ".feature",
                startWith: 1,
                iterator: iterator,
            });
        });
    </script>
    <script src="app-assets/js/repeatable.js"></script>
@endpush


@push("styles")
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
          integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endpush

@push("scripts")
    <script>
        function formatText(icon) {
            return $(`<span><i class="${$(icon.element).data('icon')}"></i>${icon.text}</span>`);
        }

        $(document).on("click", ".features .add", function () {
            $(".select-icon").select2({
                width: "50%",
                templateSelection: formatText,
                templateResult: formatText
            });
        });
        setTimeout(() => {
            $(".select-icon").select2({
                width: "50%",
                templateSelection: formatText,
                templateResult: formatText
            });
        }, 1000)
    </script>
    <script src="app-assets/js/repeatable.js"></script>
@endpush
