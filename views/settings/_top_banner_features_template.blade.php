<script type="text/template" id="features-template">
    <div class="feature">
        <div class="row d-flex align-items-center">
            <div class="col-md-10">
                <div class="tabbable">
                    <ul class="nav nav-tabs">
                        @foreach (getAllowedLang() as $k => $locale)
                            <li class="nav-item">
                                <a style="font-family: system-ui"
                                   class="{{ $loop->iteration == 1 ? 'active' : '' }} nav-link"
                                   href="#tab-{{$k}}-{?}" data-toggle="tab">
                                    {{$locale['native']}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="tab-content">
                    @foreach (getAllowedLang() as $key => $locale)
                        <div class="tab-pane {{  $loop->first ? 'active' : '' }}" id="tab-{{$key}}-{?}">
                            <div class="nav-tabs">
                                <div class="form-group">
                                    <label class="control-label col-lg-12">@lang("Title") </label>
                                    <div class="col-lg-12">
                                        <input class="form-control" name="features[{?}][{{$key}}][title]"
                                               placeholder="{{ __("Title") }}"
                                               value="{{old("features.{?}.title") }}"
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="form-group ml-1">
                    <label for="features_{?}_icon" class="control-label col-lg-12">@lang("Icon")</label>
                    <select name="features[{?}][icon]" class="form-control select-icon" id="features_{?}_icon">
                        <option value="">@lang("Choose icon")</option>
                        @foreach(Tasawk\Cms\Libs\FontawesomeIcons::get() as $icon => $title)
                            <option data-icon="{{$icon}}" value="{{$icon}}">{{$title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-2">
                <br>
                <button type="button" class="btn btn-danger span-2 delete">
                    @lang('Remove')
                </button>
            </div>
        </div>

    </div>
</script>
