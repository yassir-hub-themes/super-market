@extends("theme::layouts.master")
@section('page_title','Categories')
@push("styles")
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css">
@endpush
@section("content")
    <div class="product-category">
        <div class="container">
            <div class="subcategory-cont">
                <div class="subcategory-section">
                    <div class="row">
                        <div class="col-sm-3 hidden-xs">
                            @php
                                $categories = \Tasawk\Items\Models\Category::enabled()->parent()->get();
                            @endphp
                            <div class="list-group">
                                @php($categories = \Tasawk\Items\Models\Category::enabled()->parent()->get())
                                @php($selected_category = $category)


                                @foreach($categories as $list)
                                    @php($level = 0)
                                    @include("theme::components.shop.category",['category'=>$list ])

                                @endforeach
                            </div>
                        </div>
                        <div id="content" class="col-sm-9">
                            <h2 class="content-head">{{$selected_category->title}}</h2>
                            <div class="row sort-row hidden">
                                <div class="col-md-3 col-sm-6">
                                    {{--                                    <div class="compare-txt">--}}
                                    {{--                                        <a href="#" id="" class="btn btn-link">Compare Products (0)</a>--}}
                                    {{--                                    </div>--}}
                                </div>
                                <div class="col-md-9 col-xs-12 hidden">
                                    <div class="filter-drop required">
                                        <label>Sort by:</label>
                                        <select id="Suggestion" class="selectpicker">
                                            <option value="" disabled selected>default</option>
                                            <option value="bmw">bmw</option>
                                            <option value="saab">Saab</option>
                                            <option value="vw">VW</option>
                                            <option value="audi">Audi</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="product-section">
                                <div class="container-fluid">
                                    <div class="product-cont category-sub-product">
                                        <div class="product-cont content-section">
                                            <div class="product-container">
                                                @each('theme::products.list.product', $lists, 'product')
                                            </div>
                                        </div>
                                        {{$lists->links()}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push("scripts")
    <script src="{{Ecommerce::theme()->asset('js/bootstrap-select.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.umd.js"></script>
@endpush
