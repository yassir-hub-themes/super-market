@extends("theme::layouts.master")
@php($page_title = isset($page_title)? "- $page_title" : '')
@section('page_title',__("Shop $page_title"))
@section("content")
    <div class="content-section">
        <div class="container">
            <div class="row">
                <div id="content" class="col-sm-12">
                    <h1 class="content-head">@lang($page_title)</h1>
                    <div class="product-container">
                        @each('theme::products.list.product', $items, 'product')
                    </div>
                    {{$items->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
