@extends("theme::layouts.master")
@section('page_title',$item->title .' - ' . __('Shop'))
@push("styles")
    <link rel="stylesheet" href="{{Ecommerce::theme()->asset('css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.css">
@endpush
@section("breadcrumb")
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="/">@lang("Home")</a></li>
            <li><a href="{{route('shop.show',[Str::plural($item->type),$item->id])}}">{{$item->title}}</a></li>
        </ul>
    </div>
@endsection
@section("content")
    <div class="product-category">
        <div class="container">
            <div class="subcategory-cont">
                <div class="subcategory-product">
                    <div class="row product-row">
                        <div class="col-sm-6">
                            <div class="row ">
                                @php($otherImages = $item->getOtherImages())
                                @if($otherImages->count())
                                    <div class="col-sm-1 hidden-xs custom-padding">
                                        @foreach($otherImages as $image)
                                            <div class="product-thumbnail">
                                                <a class="thumbnail" role="button" data-fancybox="gallery"
                                                   href="{{ upload_storage_url($image)}}"
                                                   title="{{$item->title}}">
                                                    <img src="{{ upload_storage_url($image)}}"
                                                         alt="{{$item->title}}">
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                                <div class="col-sm-11 col-xs-12 main-thumbnails">
                                    <a class="thumbnail product-mainimg active-img" data-fancybox="gallery"
                                       href="{{$item->img_url}}" title="{{$item->title}}">
                                        <img class="img-responsive" src="{{$item->img_url}}"
                                             alt="{{$item->title}}">
                                        @if($item->hasOffer())
                                            <span class="sale">
                                                    %{{$item->pricing->offerRatio()}}
                                        </span>
                                        @endif
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 subcategory-product-cont">
                            <div class="subcategory-head">
                                <h2>{{$item->title}} </h2>
                            </div>
                            <ul class="list-unstyled type-list">
                                <li>@lang("Availability")
                                    : {{$item->isOutOfStock ? __('Out of stock') : __('In stock')}}</li>
                            </ul>
                            <div class="product_flexbox">
                                <ul class="list-unstyled list-inline prodprices">
                                    @if($item->hasOffer())
                                        <li>
                                            <h3 class="product-first-price">  {{ Ecommerce::formatPrice($item->pricing->originalPrice()) }} {{Ecommerce::currentSymbol()}}</h3>
                                        </li>

                                        <li>
                                            <h3 class="product-last-price"> {{ Ecommerce::formatPrice($item->pricing->finalPrice())}} {{Ecommerce::currentSymbol()}}</h3>
                                        </li>
                                    @else
                                        <li>
                                            <h3 class="product-last-price">  {{ Ecommerce::formatPrice($item->pricing->finalPrice()) }} {{Ecommerce::currentSymbol()}}</h3>
                                        </li>

                                    @endif
                                </ul>
                            </div>
                            @include('theme::products.product.unit-price',['product'=>$item])
                            @include("theme::components.shop.item_form_actions")
                            <hr>
                            <div class="rating">
                                <p>
                                <div class="rate">
                                    @include('theme::components.shop.rating',['rate'=>$item->averageRate()])
                                </div>
                                <a href=""
                                   class="rating-commit">@lang('(:number) Review',['number'=>$item->comments()->count()])</a>
                                / <a href="#" id="comment-link" class="rating-commit">@lang('Write review ?')</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs product-tabs">
                        <li class="active">
                            <a href="#tab-description" data-toggle="tab">@lang("Details")</a>
                        </li>
                        @if($item->attributes->count())
                            <li>
                                <a href="#tab-specification" data-toggle="tab">@lang("Specification")</a>
                            </li>
                        @endif
                        <li>
                            <a href="#tab-review"
                               data-toggle="tab">@lang('(:number) Review',['number'=>$item->comments()->count()])</a>
                        </li>
                    </ul>
                    <div class="tab-content proddtls">
                        <div class="tab-pane @if(!$errors->any()) active @endif" id="tab-description">
                            {!! $item->description !!}
                        </div>
                        @if($item->attributes->count())
                            <div class="tab-pane" id="tab-specification">
                                @include("theme::components.shop.specification")
                            </div>
                        @endif
                        <div class="tab-pane @if($errors->any()) active @endif" id="tab-review">
                            @include("theme::components.shop.comments")
                            @include("theme::components.shop.review_form")

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push("scripts")
    <script src="{{Ecommerce::theme()->asset('js/bootstrap-select.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui/dist/fancybox.umd.js"></script>
@endpush
