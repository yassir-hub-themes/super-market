@foreach($category->children as $child)
    <li>
        <a class="drop-link" href="{{route('shop.category.products',$child->id)}}">
            {{$child->title}}
            @if($child->children->count())
                <span role="button"
                      data-toggle="collapse"
                      data-target="#category_{{$child->id}}">
                <i class="fas fa-chevron-down"></i>
            </span>
            @endif
        </a>
        @if($child->children->count())
            <ul id="category_{{$child->id}}" class="drop-list fixall collapse" aria-expanded="true" style="">
                @foreach($child->children as $_child)
                    @include("theme::components.sidebar_categories",['category'=>$child])
                @endforeach

            </ul>
        @endif
    </li>
@endforeach
