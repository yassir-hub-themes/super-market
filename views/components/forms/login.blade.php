{{html()->form('post',route("login.check"))->class('signin-form')->id('login-form')->open()}}
<div class="signin-group">
    @php($auth_way = Ecommerce::settings()->get("ecommerce-login-by"))
    @if (in_array($auth_way ,[ "phone-only" ,'phone-and-password']))

        @include("theme::components.phone_input")
    @else
        {{html()->email('object')->class('signin-input')->placeholder(trans('Email'))->required()}}
    @endif
    @if(in_array($auth_way,["phone-and-password","email-and-password"]))
        {{html()->password('password')->class('form-control signin-input')->placeholder(trans("Password"))->required()}}
    @endif
    {{html()->button()->type("submit")->class('btn btn-primary')->text(trans("Login"))}}
</div>
{{html()->form()->close()}}
