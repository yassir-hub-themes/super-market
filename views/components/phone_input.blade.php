@php($countries = Tasawk\Locations\Models\Country::enabled()->get())
<div class="form-margin required" style="height: 44px">
    <div class="mt-2">
        @if(isset($label))
            <label class="col-sm-2 control-label" for="input-firstname">@lang($label)</label>
        @endif
        <div class="@if(isset($label)) col-md-10  @endif tel-phone-input-container">
            <input tabindex="1" type="tel" name="phone" value="{{ old('phone',$value??'') }}"
                   class="form-control tel-phone-input"
                   id="phone"
                   autocomplete="new-password"

            >
            <input type="hidden" name="full-phone"
                   class="full-phone"
                   value="{{ old('full-phone','') }}">
            <input type="hidden" name="country-code" class="country-code"
                   value="{{old('country-code',$countries->first()?$countries->first()->iso2:tenant()->account->country)}}">
            @error('phone')
            <span class="error-text-alert ml-4 mt-1">{!!   $message !!}</span>
            @enderror
{{--            @if($errors->has("phone"))--}}
{{--                <ul>--}}
{{--                    <li>@lang("It seem like you are registered before") <a href="{{route('forget-password.index')}}">@lang("Rest password")</a></li>--}}
{{--                    <li>@lang("For more detail contact administrator")</li>--}}
{{--                </ul>--}}
{{--            @endif--}}
        </div>
    </div>
</div>
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/js/intlTelInput-jquery.js"
            integrity="sha512-xo8nGg61671g6gPcRbOfQnoL+EP5SofzlUHdZ/ciHev4ZU/yeRFf+TM5dhBnv/fl05vveHNmqr+PFtIbPFQ6jw=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        ;(function () {

            $(document).ready(function () {
                $("#user_check input").click(function () {
                    $('.password-info').css('display', $(this).val().trim() == 'guest' ? 'none' : 'block')
                });
                $(".tel-phone-input").intlTelInput({
                    separateDialCode: true,
                    initialCountry: '{{old('country-code',)}}',
                    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/js/utils.min.js",
                    onlyCountries: @json($countries->isEmpty() ? [tenant()->account->country] : $countries->pluck('iso2')->toArray()),
                });
                $(".tel-phone-input").on('countrychange', function () {
                    let phone = $(this);
                    phone.parents(".tel-phone-input-container").find(".country-code").val(phone.intlTelInput("getSelectedCountryData").iso2);
                    phone.parents(".tel-phone-input-container").find(".full-phone").val(phone.intlTelInput("getNumber"));
                });
                $(".tel-phone-input").on('change load', function () {
                    let phone = $(this);
                    phone.parents(".tel-phone-input-container").find(".country-code").val(phone.intlTelInput("getSelectedCountryData").iso2);
                    phone.parents(".tel-phone-input-container").find(".full-phone").val(phone.intlTelInput("getNumber"));
                });
            });
        })();
    </script>
@endpush
