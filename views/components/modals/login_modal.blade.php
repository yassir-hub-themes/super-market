<div class="modal fade login-modal" id="login-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> @lang('Login')</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <i class="fas fa-times"></i>
                </button>
            </div>
            <div class="modal-footer">
                @include("theme::components.forms.login")
                {{html()->a(route('forget-password.index'))->class('forget-pass')->text(trans('Did you forget your password'))}}
                <p class="register-text">
                    @lang("You don't have an account.")
                    {{html()->a(route('register'))->text(trans('Register now'))}}
                </p>
            </div>
        </div>
    </div>
</div>
@push("scripts")
    <script>
        $('#login-form').on('submit', function (e) {
            e.preventDefault();
            let form = $(this);
            $.post(form.attr('action'), form.serialize(), function (data) {
                if (data.status === 0) {
                    notify("@lang('Error')", data.message, 'error');
                } else {
                    notify("@lang('Done')", data.message);
                    window.location.href = data.redirect;
                }
            }).fail(function (data) {
                if (data.status === 422) {
                    var errors = $.parseJSON(data.responseText).errors;
                    $.each(errors, function (key, value) {
                        notify("@lang('Error')", value[0], 'error');
                    });
                }
            });
        });
    </script>
@endpush

