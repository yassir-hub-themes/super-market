@php
    $cities = $countries->count() ==1?$countries->first()->cities:collect([]);
    $cities = $cities->map(function ($city) {
        return html()
            ->option($city->name, $city->id)
            ->attribute('data-lat', $city->latitude)
            ->attribute('data-lng', $city->longitude);
        });
@endphp

<h2 class="page-title">@lang('Delivery address information') </h2>
@if(Ecommerce::auth()->check())
    <div class="delivery-time-content" style="display: block">
        <div class="delivery-time">
            <label class="delivery-item">
                <input type="radio" name="address_info" id="existing_address" value="existing_address"
                       @if(old('address_info') == "existing_address") checked @endif>
                <div class="delivery-item-text">
                    <div class="text">
                        <i class="fas fa-address-book"></i>
                        @lang("Select existing address")
                    </div>
                </div>
            </label>
            <label class="delivery-item">
                <input type="radio" name="address_info" id="new_address" value="new_address"
                       @if(old('address_info') == "new_address") checked @endif>
                <div class="delivery-item-text">
                    <div class="text">
                        <i class="fas fa-plus"></i>
                        @lang("New one")
                    </div>
                </div>
            </label>
        </div>
        @error("address_id")
        <p class="text-center">
            {{html()->span()->class('error-text-alert')->html($message)}}
        </p>
        @endError
        @error("address_info")
        <p class="text-center">
            {{html()->span()->class('error-text-alert')->html($message)}}
        </p>
        @endError
    </div>
@else
    <input type="hidden" name="address_info" id="new_address" value="new_address"
           @if(old('address_info') == "new_address") checked @endif>
@endif

<div class="row new-address-container"
     @if(old('address_info') != "new_address" && Ecommerce::auth()->check() )style="display:  none @endif">
    @if($countries->count() > 1)
        <div class="col-md-4">
            <div class="float-field">
                @error('country_id')
                <span class="error-text-alert">
                {{ $message }}
            </span>
                @enderror
                <select class="new-address-select country-select select2" name="country_id"
                        data-placeholder="@lang("Select country")">
                    @foreach ($countries as $country)
                        <option value=""></option>
                        <option
                                data-lat="{{$country->latitude}}"
                                data-lng="{{$country->longitude}}"
                                @if(old("country_id") == $country->id) selected
                                @endif value="{{ $country->id }}">{{ $country->name }}</option>
                    @endforeach
                </select>
                {{html()->label(trans("Country"))->child(html()->span('*')->class('text-danger'))}}
            </div>
        </div>
    @else
        {{html()->hidden("country_id")->value($countries->first()->id)}}
    @endif

    <div class="col-md-4">
        <div class="float-field">
            @error('city_id')
            {{html()->span()->class('error-text-alert')->html($message)->style(['position'=> 'absolute', "bottom"=> '0'])}}
            @enderror
            {{html()
                ->select('city_id')
                ->class('new-address-select city-select select2')
                ->options([null=>''])
                ->children($cities)
                ->disabledIf($countries->count()>1)
                ->value(old('city_id'))
                ->data('placeholder',trans("Select city"))}}

            {{html()->label(trans("City"))->child(html()->span('*')->class('text-danger'))}}
        </div>
    </div>
    <div class="col-md-4">
        <div class="float-field">
            @error('address.state')
            {{html()->span()->class('error-text-alert')->html($message)->style(['position'=> 'absolute', "bottom"=> '0'])}}
            @enderror
            {{html()->text("address[state]")->placeholder(trans(('State')))->value(old('address.state'))}}
            {{html()->label(trans("State"))}}
        </div>
    </div>

    <div class="col-md-4">
        <div class="float-field">
            @error('address.street')
            {{html()->span()->class('error-text-alert')->html($message)->style(['position'=> 'absolute', "bottom"=> '0'])}}
            @enderror
            {{html()->text("address[street]")->placeholder(trans(('Street name')))->value(old('address.street'))}}
            {{html()->label(trans("Street name"))}}

        </div>
    </div>
    <div class="col-md-4">
        <div class="float-field">
            @error('address.building_number')
            {{html()->span()->class('error-text-alert')->html($message)->style(['position'=> 'absolute', "bottom"=> '0'])}}
            @enderror
            {{html()->text("address[building_number]")->placeholder(trans(('Building number')))->value(old('address.building_number'))}}
            {{html()->label(trans("Building number"))}}
        </div>
    </div>
    <div class="col-md-4">
        <div class="float-field">
            @error('address.apartment_number')
            {{html()->span()->class('error-text-alert')->html($message)->style(['position'=> 'absolute', "bottom"=> '0'])}}
            @enderror
            {{html()->text("address[apartment_number]")->placeholder(trans(('Apartment number')))->value(old('address.apartment_number'))}}
            {{html()->label(trans("Apartment number"))}}
        </div>
    </div>
    <div class="col-md-12">
        <div class="float-field">
            @error('address.note')
            {{html()->span()->class('error-text-alert')->html($message)->style(['position'=> 'absolute', "bottom"=> '0'])}}
            @enderror
            {{html()->textarea("address[note]")->placeholder(trans(('Note')))->value(old('address.note'))->rows(8)}}
            {{html()->label(trans("Note"))}}
        </div>
    </div>

</div>

<div class="checkout-map "
     @if(old('address_info') != "new_address" && Ecommerce::auth()->check() )style="display:  none @endif">
    <p style="margin: 0;color:var(--danger)"><i
                class="fas fa-exclamation-triangle"></i> @lang("Please enable automatically location or select on the map")
    </p>


    <div id="checkout-map" style="height:100%;width: 100%"></div>
    <a role="button" class="locationButton @if(!old('address.address_name')) shake shake-once @endif"
       data-toggle="tooltip" data-placement="left" title="@lang('Click to find your location automatic')">
        <img src="{{ Ecommerce::theme()->asset('images/location.png') }}">
    </a>
    <div class="locationInput-container" style="left: 35px;right: 70px">
        <input type="text" class="locationInput" readonly value="{{old('address.address_name')}}">
        {{--        @auth(Ecommerce::auth()->guard())--}}
        {{--            <a role="button" class="adressBook" data-toggle="modal" data-target="#addressBook-modal"--}}
        {{--            >--}}
        {{--                <span style="font-size: 16px" class="d-inline-block mx-2">@lang("Saved addresses")</span>--}}
        {{--                <i class="fas fa-address-book"></i>--}}
        {{--            </a>--}}
        {{--        @endauth--}}
    </div>
    <?php
    $address_lat = old('address.lat', '24.774255');
    $address_lang = old('address.lang', '46.737586534');
    ?>
    <input type="hidden" name="address_id" id="address_id" value="{{old('address_id')}}">
    <input type="hidden" name="address[lat]" id="map-lat" value="{{$address_lat}}">
    <input type="hidden" name="address[lang]" id="map-lng" value="{{$address_lang}}">
    <input type="hidden" name="address[address_name]" id="locationInput" value="{{old('address.address_name')}}">
</div>
@push('styles')

@endpush
@push('scripts')
    {{Utilities::MapScriptTag()}}
    <script>
        ;(function () {
            function initMap() {
                const locationButton = document.getElementsByClassName('locationButton')[0];
                locationButton.addEventListener("click", () => {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(
                            (position) => {
                                const pos = {
                                    lat: position.coords.latitude,
                                    lng: position.coords.longitude,
                                };
                                marker.setPosition(pos);
                                map.setCenter(pos);
                                handleMarkerOutsideCountry(pos.lat, pos.lng);
                                updateInput(pos)
                            },
                        );
                    }
                });
                google.maps.event.addListener(marker, "dragend", function (e) {
                    handleMarkerOutsideCountry(e.latLng.lat(), e.latLng.lng());
                    updateInput(e.latLng);
                });
                google.maps.event.addListener(map, "click", function (e) {
                    const pos = {
                        lat: e.latLng.lat(),
                        lng: e.latLng.lng(),
                    };
                    marker.setPosition(pos);
                    handleMarkerOutsideCountry(e.latLng.lat(), e.latLng.lng());
                    updateInput(e.latLng);
                });
            }

            function updateInput(latLng) {
                let geocoder = new google.maps.Geocoder();
                $('#map-lat').val(latLng.lat);
                $('#map-lng').val(latLng.lng);
                geocoder.geocode({
                    'latLng': latLng
                }, function (results) {
                    $('#locationInput').val(results[0].formatted_address);
                    document.getElementsByClassName('locationInput')[0].value = results[0].formatted_address
                });
            }

            function setMarkerOnMap(lat, lng, map, zoom = 5) {
                if (!(lat && lng)) {
                    return;
                }
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    map: map,
                    draggable: true,
                    icon: "{{Ecommerce::theme()->asset('images/pin.png')}}"
                });
                google.maps.event.addListener(marker, "dragend", function (e) {
                    errors = 0;
                    updateInput(e.latLng);
                    handleMarkerOutsideCountry(e.latLng.lat(), e.latLng.lng());
                });
                markers.push(marker);
                map.setCenter(new google.maps.LatLng(lat, lng));
                map.setZoom(zoom);
                map.panTo(marker.position);
                updateInput(marker.position);
            }

            function handleMarkerOutsideCountry(lat, lng) {
                $(".map-error-msg").html("");
                let api_key = "{{settings()->group('third-party')->get('google_map_key', env("GOOGLE_MAP_KEY"))}}";
                let selectedCountry = $(".city-select").find("option:selected");
                selectedCountry = selectedCountry.val() !== "" ? selectedCountry.text() : null;
                if (!selectedCountry) {
                    return;
                }
                fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lng}&key=${api_key}&language={{app()->getLocale()}}`)
                    .then(response => response.json())
                    .then(data => {
                        let country = data.results[0].address_components.filter(function (location) {
                            return location.types.includes("country");
                        })[0];
                        if (typeof country == "undefined") {
                            $(".map-error-msg").text("@lang('Please choose a more accurate area')");
                            errors++;
                        } else if (selectedCountry !== country.long_name) {
                            $(".map-error-msg").text("@lang('Sorry this specified area out of the zone')");
                            errors++;
                        }
                        // let country_long_name_val = $(`.country-select option:contains(${country.long_name})`).val();
                        // if (country_long_name_val != $('.country-select').val()) {
                        //     $('.country-select').val(country_long_name_val).change()
                        // }

                    });
            }

            function restMap() {
                for (let i = 0; i < markers.length; i++) {
                    markers[i].setMap(null);
                }
            }

            let errors = 0;
            let markers = [];
            $('.shake-once').hover(function () {
                $(this).removeClass('shake shake-once')
            });
            const myLatLng = {
                lat: {{$address_lat}},
                lng: {{$address_lang}}
            };
            $(document).ready(function () {
                initMap();
            });
            let map = new google.maps.Map(document.getElementById("checkout-map"), {
                center: myLatLng,
                zoom: 17,
            });
            let marker = new google.maps.Marker({
                position: map.getCenter(),
                draggable: true,
                map,
                icon: "{{Ecommerce::theme()->asset('images/pin.png')}}"
            });
            markers.push(marker);
            $('[name="adressbook"]').on('change', function () {
                let info = JSON.parse($(this).attr('data-info'));
                console.log(info);
                setupAddressPredefinedData(info);
                setupMap(info.map_location);
            });

            function setupAddressPredefinedData(predefinedData) {
                console.log(predefinedData);

                let items = {
                    id: '#address_id',
                    country_id: '[name="country_id"]',
                    city_id: '[name="city_id"]',
                    state: '[name="address[state]"]',
                    street: '[name="address[street]"]',
                    building_number: '[name="address[building_number]"]',
                    apartment_number: '[name="address[apartment_number]"]',
                    note: '[name="address[note]"]',

                };
                for (const item in items) {
                    if (predefinedData.hasOwnProperty(item)) {
                        let el = $(items[item]);
                        if (el.length) {
                            el.val(predefinedData[item]).change()
                        }
                    }
                }
            }

            function setupMap(location) {
                $('#map-lat').val(parseFloat(location.lat));
                $('#map-lng').val(parseFloat(location.lng));
                $('#locationInput').val(location.address);
                const pos = {
                    lat: parseFloat(location.lat),
                    lng: parseFloat(location.lng),
                };
                marker.setPosition(pos);
                map.setCenter(pos);
            }

            $(".country-select").on("change", function () {
                let country = $(this).val();
                let select = $(".city-select");
                let lat = $(this).find("option:selected").data("lat");
                let lng = $(this).find("option:selected").data("lng");
                select.empty().attr("disabled", "disabled");
                $.ajax({
                    url: route("ajax.countries.country.cities", country),
                    success: function (data) {
                        applyDeliveryServiceByArea(country, "country");
                        select.html(data).removeAttr("disabled");
                        let oldCityId;
                        @if(old("city_id"))
                            oldCityId = select.find('[value="{{old("city_id")}}"]');
                        oldCityId.attr('selected', 'selected');
                        lat = oldCityId.data("lat");
                        lng = oldCityId.data("lng");
                        @endif
                        @if(!old("city_id"))
                        restMap();
                        setMarkerOnMap(lat, lng, map, 10);
                        @endif
                    }
                });
            });
            $(document).on("change", ".city-select", function () {
                let city = $(this).val(),
                    lat = $(this).find("option:selected").data("lat"),
                    lng = $(this).find("option:selected").data("lng");
                @if(!old("city_id"))
                restMap();
                setMarkerOnMap(lat, lng, map, 10);
                @endif
                applyDeliveryServiceByArea(city);
                updateDeliveryTimes(city);
            });
            @if(old("country_id"))
            $(".country-select").trigger("change");
            @endif
            $(document).on("change click", '[name="address_info"]', function () {
                if ($(this).val() === "existing_address") {
                    $(".new-address-container, .checkout-map").slideUp(function () {
                        setTimeout(() => {
                            $("#addressBook-modal").modal("show");
                        }, 500);

                    });

                } else {
                    $(".new-address-container, .checkout-map").slideDown("hidden");
                    $("#address_id").val(null)
                    $('[name="adressbook"]').prop("checked", false);
                    setupAddressPredefinedData({
                        id: null,
                        state: null,
                        street: null,
                        building_number: null,
                        apartment_number: null,
                        note: null,

                    })

                }
            });
        })();
    </script>
@endpush
