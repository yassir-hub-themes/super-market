<?php
$payments = settings()->group("ecommerce")->json("allowed_payment_methods");
$paymentProviders = app('payment')->enabledProviders();
if (empty($paymentProviders)) {
    return;
}
?>
<div class="payment-methods-content">
    <h2 class="page-title">
        @lang('Select payment method')
    </h2>
    <div class="payment-methods">
        @foreach ($paymentProviders as $provider)
            <label class="payment-method" data-toggle="tooltip" data-placement="top"
                   title="{{$provider->title()}}">
                <input type="radio" name="payment_method" @if(old('payment_method') == $provider->id()) checked @endif value="{{ $provider->id() }}">
                <div class="payment-img loading-img">
                    <img class="lazy-img"
                         alt="{{$provider->title()}}"
                         src="{{ $provider->logo() }}">
                </div>
            </label>
        @endforeach
    </div>
    @error('payment_method')
    <p class="text-center">
        <span class="error-text-alert">{{ $message }}</span>
    </p>
    @enderror
</div>
