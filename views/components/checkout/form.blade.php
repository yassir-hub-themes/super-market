<section class="content-section checkout-content">
    <div class="container">
        <form action="{{ route('checkout.store') }}" method="POST" id="checkout-store" autocomplete="off">
            @csrf
            @if (!Ecommerce::auth()->check() && $registration_enabled )
                @include('theme::components.checkout.register')
            @endif
            @if(!$registration_enabled && !Ecommerce::auth()->check())
                <p class="page-title">
                    @lang('Please')
                    <a href="{{route('login.form')}}">@lang('Log in')</a> /
                    <a href="{{route('register')}}">@lang('Register')</a>
                    @lang('To complete your order')</p>
            @endif
            @include("theme::components.checkout.map")
            @include('theme::components.checkout.payment-methods')
            @include('theme::components.checkout.delivery-time',['opening_days'=>$opening_days])
            <a class="cart-btn">
                @lang('Checkout')
            </a>
        </form>
    </div>
</section>
