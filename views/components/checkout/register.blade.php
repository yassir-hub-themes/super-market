<h2 class="page-title">
    @lang('Enter your information')
</h2>
<div class="row">
    <div class="col-12 col-md-6  phone required">
        <label>
            @lang('Phone')
        </label>
        <div class="input-container">
            <input tabindex="1" type="tel" name="user[phone]" value="{{ old('user.phone','') }}"
                   class="form-control"
                   id="phone"
                   autocomplete="new-password">
            <input type="hidden" name="full-phone" id="full-phone" value="{{ old('full-phone','') }}">
            <input type="hidden" name="country-code" id="country-code"
                   value="{{old('country-code',tenant()->account->country)}}">
        </div>
        @error('user.phone')
        <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
        @enderror
    </div>
    <div class="col-12 col-md-6  name required">
        <label>
            @lang('Full name')
        </label>
        <input type="text" value="{{ old('user.name','') }}" name="user[name]" class="form-control"
               placeholder="@lang('Full name')"
               autocomplete="new-password">
        @error('user.name')
        <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
        @enderror
    </div>
    <div class="col-12 col-md-6  email">
        <label>
            @lang('Email')
        </label>
        <input type="email" value="{{ old('user.email','') }}" name="user[email]" class="form-control"
               autocomplete="new-password" placeholder="@lang('Email')">
        @error('user.email')
        <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
        @enderror
        <div class="mt-2 ml-4" id="user_check">
            <label>
                @lang('As client')
                <input type="radio" value="user" @if(old('user.user_check') == 'user') checked
                       @endif name="user[user_check]">
            </label>
            <label>
                @lang('As guest')
                <input type="radio" value="guest" @if(old('user.user_check','guest') == 'guest') checked
                       @endif name="user[user_check]">
            </label>
        </div>
    </div>
    <div class="col-12 col-md-6 password password-info required"
         style="display: @if(old('user.user_check','guest') == 'guest') none @endif">
        <label>
            @lang('Password')
        </label>
        <input type="password" name="user[password]" class="form-control" autocomplete="new-password">
        @error('user.password')
        <span class="error-text-alert ml-4 mt-1">{{ $message }}</span>
        @enderror
    </div>
</div>
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/css/intlTelInput.css"
          integrity="sha512-gxWow8Mo6q6pLa1XH/CcH8JyiSDEtiwJV78E+D+QP0EVasFs8wKXq16G8CLD4CJ2SnonHr4Lm/yY2fSI2+cbmw=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <style>
        #phone,
        .iti {
            direction: ltr;
            text-align: left;
        }

        .iti--allow-dropdown .iti__flag-container .iti__selected-flag,
        .iti--allow-dropdown .iti__flag-container:hover .iti__selected-flag {
            background-color: transparent;
        }

        .iti.iti--allow-dropdown {
            width: 100%;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/js/intlTelInput-jquery.js"
            integrity="sha512-xo8nGg61671g6gPcRbOfQnoL+EP5SofzlUHdZ/ciHev4ZU/yeRFf+TM5dhBnv/fl05vveHNmqr+PFtIbPFQ6jw=="
            crossorigin="anonymous"
            referrerpolicy="no-referrer"></script>
    <!--suppress JSUnresolvedFunction -->
    <script>
        ;(function () {
            $(document).ready(function () {
                $("#user_check input").click(function () {
                    $('.password-info').css('display', $(this).val().trim() == 'guest' ? 'none' : 'block')
                });
                $("#phone").intlTelInput({
                    separateDialCode: true,
                    initialCountry: '{{old('country-code',)}}',
                    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.15/js/utils.min.js",
                    onlyCountries: @json($countries->isEmpty() ? [tenant()->account->country] : $countries->pluck('iso2')->toArray()),
                });
                $("#phone").on('countrychange', function () {
                    $("#country-code").val($("#phone").intlTelInput("getSelectedCountryData").iso2);
                    $("#full-phone").val($("#phone").intlTelInput("getNumber"));
                });
                $("#phone").on('change load', function () {
                    $("#country-code").val($("#phone").intlTelInput("getSelectedCountryData").iso2);
                    $("#full-phone").val($("#phone").intlTelInput("getNumber"));
                });
            });
        })();
    </script>
@endpush
