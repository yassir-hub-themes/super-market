@foreach ($shifts as $_shift)
@php($delivery_value = $_shift['from'].' - '.$_shift['to'])
<div class="pb-2">
    <div>@lang('From :FROM to :TO',['FROM'=>$_shift['from'],'TO'=>$_shift['to']])</div>
    <div class="hours">
        @foreach($_shift['hours'] as $hour)
        <label class="time-range">
            <input type="radio"
                   data-shift-id="{{$_shift['id']}}"
                   @if(old('delivery_time') == $hour) checked @endif
            class="delivery_time_select" value="{{$hour}}"
            name="time-range">
            <span class="time-text">{{$hour}}</span>
        </label>
        @endforeach
    </div>
</div>
@endforeach
