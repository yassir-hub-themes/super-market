@if(!empty($shifts))
    <div tabindex="-1" class="modal fade date-modal" id="date-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">@lang('Choose delivery day')</h5>
                    <div class="date-picker-container">
                        <input type="date" id="delivery_date_select" class="delivery-date-input"
                               value="{{old('delivery_date',date('Y-m-d'))}}"
                               data-default-date="{{old('delivery_date',date('Y-m-d'))}}">
                        <span class="fas fa-calendar-alt"></span>
                    </div>
                </div>
                <div class="modal-body">
                    <h5 class="modal-title pb-3">@lang('Select a period')</h5>
                    <div class="times-list text-center" id="times-list">
                        @include("theme::components.checkout.modals.times-list", ['shifts' => $shifts])
                    </div>
                </div>
                <div class="modal-footer">
                    <a role="button" data-dismiss="modal" class="continue-btn select-delivery">
                        @lang('Continue')
                    </a>
                </div>
            </div>
        </div>
    </div>
    @push('scripts')
        <?php
        $disable_current_day_in_delivery_date = settings()->group('delivery_times_and_shifts')->bool('disable_current_day_in_delivery_date');
        ?>
        <script>
            ;(function (window, document, $) {
                if ($(window).width() > 1199) {
                    let opening_days = @json($opening_days);
                    let defs = @json($defs);

                    function disabled_days(date) {
                        return !Object.values(opening_days).includes(defs['_' + date.getDay()]);
                    }

                    $(".delivery-date-input").flatpickr({
                        locale: '{{app()->getLocale()}}',
                        minDate: new Date({{$firstAvailableInWeek}}),
                        dateFormat: "d m Y",
                        ariaDateFormat:"d m Y",
                        disable: [disabled_days, @if($disable_current_day_in_delivery_date) '{{date('d M Y')}}' @endif],
                        defaultDate: @if(old('delivery_date')) '{{date('d M Y',strtotime(old('delivery_date')))}}'
                        @else new Date({{$firstAvailableInWeek}}) @endif,
                        onChange: function (selectedDates, dateStr, instance) {
                            $('#delivery_date').val(flatpickr.formatDate(selectedDates[0], "Y-m-d"));
                        },
                    });
                    @if($disable_current_day_in_delivery_date)
                    setTimeout(function () {
                        let enabledDay = flatpickr.parseDate($('.flatpickr-day').not('.flatpickr-disabled').eq(0).attr('aria-label'), "d m Y");
                        $(".delivery-date-input")[0]._flatpickr.setDate(enabledDay);
                        $(".delivery-date-input")[0]._flatpickr.set('defaultDate',enabledDay);
                    });
                    @endif
                }
            })(window, document, jQuery)
        </script>
    @endpush
@endif
