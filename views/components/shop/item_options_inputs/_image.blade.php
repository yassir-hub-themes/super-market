@php
    $option_values = $option->values()->pluck("name","id");
@endphp
<div class="float-field">
    <div class="float-field">
        <input type="file" name="option[option_{{$option->id}}]" accept="image/*"/>
    </div>
    @error("option_$option->id")
    <p class="text-danger">{{$message}}</p>
    @enderror
    <label>
        {{$option->name}}
        @if($option->pivot->required)
            <span class="error-text-alert">*</span>
        @endif
    </label>
</div>
