@php
    $item_options_pivot = $option->getValuesByItem($item->id)->get()->pluck("pivot");
    $options_value = $option->values()->orderBy("sort_order","asc")->get();
@endphp
<div class="float-field">
    <label>
        {{$option->name}}
        @if($option->pivot->required)
            <span class="text-danger">*</span>
        @endif
    </label>
</div>
@foreach($options_value as $value)
    @php($item_option_value =$item_options_pivot->where("option_value_id",$value->id)->first())
    @continue(!Arr::has($item_option_value,"price"))
    <div class="form-check mb-4">
        <input id="option[option_{{$value->id}}]"
               type="radio"
               name="option[option_{{$option->id}}]"
               value="{{$value->id}}"
               class="form-check-input">
        <label for="option[option_{{$value->id}}]" class="form-check-label">
            {{$value->name}}
            <?php $final_price = Ecommerce::price($item_option_value["price"] ?? 0)->withTaxes($item->taxs->sum("cost"))->final();?>
            ({{Ecommerce::formatPrice(Ecommerce::calcCurrencyRatio($final_price))}}  {{Ecommerce::currentSymbol()}})
        </label>
    </div>
@endforeach
@error("option_$option->id")
<p class="text-danger">{{$message}}</p>
@enderror
