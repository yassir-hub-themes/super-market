<div class="float-field">
    <textarea name="option[option_{{$option->id}}]">{{old("option_{$option->id}",$option->pivot->value)}}</textarea>
    @error("option_$option->id")
    <p class="text-danger">{{$message}}</p>
    @enderror
    <label>
        {{$option->name}}
        @if($option->pivot->required)
            <span class="text-danger">*</span>
        @endif
    </label>
</div>
