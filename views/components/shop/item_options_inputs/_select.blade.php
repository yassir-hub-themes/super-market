@php
    $item_options_pivot = $option->getValuesByItem($item->id)->get()->pluck("pivot");
    $options_value = $option->values()->orderBy("sort_order","asc")->get();
@endphp
<div class="float-field">
    <select class="form-control" name="option[option_{{$option->id}}]">
        <option value="">@lang('Select')</option>
        @foreach($options_value as $value)
            <?php
            $item_option_value = $item_options_pivot->where("option_value_id", $value->id)->first();
            $final_price = Ecommerce::price($item_option_value["price"] ?? 0)->withTaxes($item->taxs->sum("cost"))->final();
            ?>
            @continue(!Arr::has($item_option_value,"price"))
            <option  value="{{$value->id}}" @if(old("option_$option->id") == $value->id) selected @endif>
                {{$value->name}}
                ({{Ecommerce::formatPrice(Ecommerce::calcCurrencyRatio($final_price))}}{{Ecommerce::currentSymbol()}})
            </option>
        @endforeach
    </select>
    @error("option_$option->id")
    <p class="text-danger">{{$message}}</p>
    @enderror
    <label>
        {{$option->name}}
        @if($option->pivot->required)
            <span class="text-danger">*</span>
        @endif
    </label>
</div>

