<div class="float-field">
    <input type="{{$option->type}}" name="option[option_{{$option->id}}]" value="{{old("option_{$option->id}",$option->pivot->value)}}"/>
    @error("option_$option->id")
    <p class="text-danger">{{$message}}</p>
    @enderror
    <label>
        {{$option->name}}
        @if($option->pivot->required)
            <span class="text-danger">*</span>
        @endif
    </label>
</div>
