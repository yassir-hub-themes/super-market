@push("styles")
    <style>.fa-star.checked {
            color: orange;
        }</style>
@endpush
<div class="rate">
    @foreach(range(5,1) as $num)
        <span class="fa fa-star @if($num<= $rate) checked @endif"></span>
    @endforeach
</div>
