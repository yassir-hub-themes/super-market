<form action="{{route('add-to-cart',$item->id)}}" method="POST"
      enctype="multipart/form-data">
    @csrf
    @include("theme::components.shop.options-inputs")
    <div class="quantity_cart">
        <label class="control-label">@lang('Quantity'):</label>

        <div class="qty-price">
            <div class="item-qty">
                <a role="button" class="qty-control disabled-actions qty-plus">
                    <i class="fas fa-plus-circle"></i>
                </a>
                <a role="button" class="qty-control disabled-actions  qty-minus">
                    <i class="fas fa-minus-circle"></i>
                </a>
                <input type="number" class="qty-input disabled-actions"
                       name="quantity"
                       value="{{Cart::getQuantityByModelId($item->id)?:$item->min_quantity}}"
                       @if($item->type !== "service" && $item->quantity > 0)
                       data-max="{{$item->quantity}}"
                       @endif
                       data-item="{{$item->id}}"
                       data-min="{{$item->min_quantity}}"
                       required>
            </div>
        </div>
        <button type="submit" id="button-cart"
                class="btn btn-primary btn-lg btn-block">
            <i class="fas fa-shopping-cart"></i>&nbsp; @lang("Add to cart")
        </button>
    </div>
</form>
