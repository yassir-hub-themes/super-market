<table class="table table-bordered">
    <tbody>
    @foreach($attribute_groups as $group=>$attributes)
        <tr>
            <td colspan="2" style="background-color: #ebebeb"><strong>{{$group}}</strong></td>
        </tr>
        @foreach($attributes as $attribute)
            <tr>
                <td>{{$attribute['title']??""}}</td>
                <td>{{$attribute['value']??""}}</td>
            </tr>
        @endforeach

    @endforeach

    </tbody>
</table>
