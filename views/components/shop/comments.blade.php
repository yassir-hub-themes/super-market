@foreach($item->comments as $comment)
    <table class="table table-striped table-bordered">
        <tbody>
        <tr class="rate-head">
            <td style="width: 50%;">
                <strong>{{ $comment->commenter->name ?? $comment->guest_name }}</strong>
            </td>
            <td class="text-right">{{ $comment->created_at->diffForHumans() }}</td>
        </tr>
        <tr>
            <td class="border-commet" colspan="2">
                <p>{{$comment->comment }}</p>
                <div style="float:{{Utilities::isRtl() ?'right':'left'}};">
                    @include('theme::components.shop.rating',['rate'=>$comment->rating])
                </div>
            </td>
        </tr>
        </tbody>
    </table>

@endforeach
