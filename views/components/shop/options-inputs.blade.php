@if($item->options->count())
    <h3 class="section-title">@lang("Available options")</h3>
    <div class="single-options">
        @foreach($item->options as $option)
            {{--Choose--}}
            @includeWhen($option->type === "select","theme::components.shop.item_options_inputs._select")
            @includeWhen($option->type === "checkbox","theme::components.shop.item_options_inputs._checkbox")
            @includeWhen($option->type === "radio","theme::components.shop.item_options_inputs._radio")
{{--            @includeWhen($option->type === "checkbox","theme::components.shop.item_options_inputs._checkbox")--}}
            {{--Input and Date--}}
            @includeWhen($option->type === "textarea","theme::components.shop.item_options_inputs._textarea")
            @includeWhen(in_array($option->type,['text','number','date']),"theme::components.shop.item_options_inputs._input")
            {{--File--}}
            @includeWhen($option->type == 'image' ,"theme::components.shop.item_options_inputs._image")
            @includeWhen($option->type == 'file' ,"theme::components.shop.item_options_inputs._file")
        @endforeach
    </div>
@endif
