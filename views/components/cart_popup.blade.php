@if(app('cart')->getTotalQuantity())
    <li>
        <table class="table table-striped">
            <tbody>
            @foreach(app('cart')->getContent() as $item)

                <tr>
                    <td class="text-center">
                        <a href="{{route('shop.show',['products',$item->associatedModel->id])}}">
                            <img src="{{ $item->associatedModel->img_url}}"
                                 alt="{{$item->name}}"
                                 title="{{$item->name}}"
                                 class="img-thumbnail">
                        </a>
                    </td>
                    <td class="text-left"><a
                                href="{{route('shop.show',['products',$item->associatedModel->id])}}">{{$item->name}}</a>
                    </td>
                    <td class="text-right">x {{$item->quantity}}</td>
                    <td class="text-right">{{Ecommerce::formatPrice($item->price)}}</td>
                    <td class="text-center">
                        <button type="button" title="@lang('Delete')"
                                class="btn btn-danger btn-xs delete-item-cart-popup" data-item="{{$item->id}}">
                            <i class="fa fa-times"></i>
                        </button>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </li>
    <li>
        <div>
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td class="text-right">
                        <strong>@lang("Subtotal")</strong>
                    </td>
                    <td class="text-right">{{Ecommerce::formatPrice(app('cart')->getSubTotal())}} {{Ecommerce::currentSymbol()}}</td>
                </tr>
                <tr>
                    <td class="text-right">
                        <strong>@lang("Total")</strong>
                    </td>
                    <td class="text-right">{{Ecommerce::formatPrice(app('cart')->getTotal())}} {{Ecommerce::currentSymbol()}}</td>
                </tr>
                </tbody>
            </table>
            <p class="text-right cartbtns">
                <a href="{{route('checkout')}}" style="width: 100%">
                    <i class="fa fa-shopping-cart"></i>@lang("Checkout")
                </a>
            </p>
        </div>
    </li>
@else
    <li>
        <p class="text-center">@lang("Shopping cart is empty !")</p>
    </li>
@endif
