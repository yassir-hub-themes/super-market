<div class="megaenu-1">
    @foreach($children as $child)
        <a class="megaenu-link" href="{{route('shop.category.products',$child->id)}}">{{$child->title}}
            ({{$child->items()->count()}})</a>
    @endforeach
</div>
