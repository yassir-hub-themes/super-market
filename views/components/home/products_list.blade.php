<div class="product-section">
    <div class="container-fluid">
        <div class="product-cont">
            <div class="product-head">
                <h2 class="product-h">@lang($title)</h2>
                <a href="{{$url}}">
                    @lang('Show more')
                </a>
            </div>
            <div class="product-container">
                @each('theme::products.list.product', $products, 'product')
            </div>
        </div>
    </div>
</div>
