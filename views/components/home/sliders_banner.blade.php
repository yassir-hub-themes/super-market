@php($banner = \Tasawk\Banners\Model\Banner::find( settings()->group('theme_super_market')->get('header_banner')))
@php($hasSliders = isset($sliders) && count($sliders))
<div class="main-section">
    <div class="container-fluid">
        <div class="main-cont" @if(!$banner) style="grid-template-columns: 1fr;" @endif>
            @if ($hasSliders)
                <div class="main-slider">
                    <div class="swiper-cont">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                @foreach ($sliders as $slider)
                                    <div class="swiper-slide">
                                        <div class="main">
                                            <a href="{{ $slider->link }}" class="pro-img">
                                                <img src="{{upload_storage_url($slider->image)}}"
                                                     class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <div class="swiper-btn-prev swiper-btn"><i class="fas fa-chevron-right"></i></div>
                        <div class="swiper-btn-next swiper-btn"><i class="fas fa-chevron-left"></i></div>
                    </div>
                </div>
            @endif
            @if($banner)
                <div class="main-img-cont">
                    <figure class="main-figure">
                        <a href="#">
                            <img src="{{upload_storage_url($banner->image)}}"
                                 class="img-responsive" alt="@lang('Banner image')">
                        </a>
                    </figure>
                </div>
            @endif
        </div>
    </div>
</div>
