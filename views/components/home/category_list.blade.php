@php
    $categories = \Tasawk\Items\Models\Category::enabled()->parent()->take(7)->get();
@endphp
<div class="product-section">
    <div class="product-categories">
        <h2 class="categories-head">
            @lang('Shop by category')
        </h2>
        <div class="categories-slider">
            <div class="swiper-container">
                <div class="swiper-wrapper">
                    @foreach($categories as $category)
                        <div class="swiper-slide">
                            <div class="categories-ancor">
                                <a class="categories-link" href="{{route('shop.category.products',$category->id)}}">
                                    {{$category->title}}
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
