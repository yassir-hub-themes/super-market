@php($banner = \Tasawk\Banners\Model\Banner::find( settings()->group('theme_super_market')->get('footer_banner')))
@if($banner)
    <section class="big-banner-section">
        <div class="container-fluid">
            <div class="big-banner-cont wow fadeInDown" wow-data-delay="5s">
                <a href="#">
                    <img src="{{upload_storage_url($banner->image)}}" alt="banner-image" class="img-responsive">
                </a>
            </div>
        </div>
    </section>
@endif
