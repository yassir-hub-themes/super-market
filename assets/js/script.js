//sidenav
function openNav() {
    $("#main").toggleClass("active");
    $("body").toggleClass("overflow");
    $(".overlay-box").fadeToggle("500");
    $(".sidenav").toggleClass("active");

}
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
$(".overlay-box").click(function () {
    $("#main").removeClass("active");
    $("body").removeClass("overflow");
    $(".overlay-box").fadeOut("500");
    $(".sidenav").removeClass("active");

});
///////fixednav
// NAVBAR STICKY
var $stickyNav = $(".navbar-sticky");

$(window).on("scroll load", function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
        $stickyNav.addClass("navbar-sticky-moved-up");
    } else {
        $stickyNav.removeClass("navbar-sticky-moved-up");
    }
    // apply transition
    if (scroll >= 50) {
        $stickyNav.addClass("navbar-sticky-transitioned");
    } else {
        $stickyNav.removeClass("navbar-sticky-transitioned");
    }
    // sticky on
    if (scroll >= 50) {
        $stickyNav.addClass("navbar-sticky-on");
    } else {
        $stickyNav.removeClass("navbar-sticky-on");
    }

});
var headerHeight = $("header").outerHeight();
$("header").css("height", headerHeight + "px");
//sidenav
$(document).ready(function () {
    new WOW().init();

    $(".logged-drop").on("click", function () {
        $(".logged-content").toggleClass("open");
        $(".overlay-box2").fadeToggle("500");
    });
    $(".overlay-box2").on("click", function () {
        $(".logged-content").removeClass("open");
        $(".overlay-box2").fadeToggle("500");
    });
    $(".drop-link span").click(function (e) {
        e.preventDefault();
    });
    // //main Slider Carousel
    ///////// ** main** /////////
    var specials = new Swiper(".main-slider .swiper-container", {
        loop: true,
        autoplay: true,
        pagination: {
            el: ".main-slider .swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".main-slider .swiper-btn-next",
            prevEl: ".main-slider .swiper-btn-prev",
        },
        breakpoints: {
            0: {
                slidesPerView: 1,
            },
            767: {
                slidesPerView: 1,
            },
            992: {
                slidesPerView: 1,
            },
            1199: {
                slidesPerView: 1,
            },
        },
    });
    // //categories Slider Carousel
    var categories = new Swiper(".categories-slider .swiper-container", {
        loop: false,
        autoplay: false,
        breakpoints: {
            0: {
                slidesPerView: 3,
                spaceBetween: 8,
            },
            767: {
                slidesPerView: 3,
                spaceBetween: 8,
            },
            992: {
                slidesPerView: 5,
                spaceBetween: 8,
            },
            1199: {
                slidesPerView: 7,
                spaceBetween: 8,
            },
        },
    });
    ///////// **product-qty** /////////

    $(".qty-input").on("change", function () {
        let qtyInput = $(this);
        let maxVal = parseInt(qtyInput.attr("data-max"));
        let minVal = parseInt(qtyInput.attr("data-min"));
        let value = qtyInput.val();
        if (value != 0 && value < minVal) {
            qtyInput.val(minVal);
            return;
        }
    });
    $(".qty-plus").on("click", function () {
        let parentElm = $(this).parents(".item-qty");
        let qtyInput = parentElm.find(".qty-input");
        let maxVal = parseInt(qtyInput.attr("data-max"));
        let minVal = parseInt(qtyInput.attr("data-min"));
        let value = qtyInput.val();
        if (value < minVal) {
            qtyInput.val(minVal).change();
            return;
        }
        if (value < maxVal || isNaN(maxVal)) {
            value++;
            qtyInput.val(value).change();
        }
    });
    $(".qty-minus").on("click", function () {
        let parentElm = $(this).parents(".item-qty");
        let qtyInput = parentElm.find(".qty-input");
        let minVal = parseInt(qtyInput.attr("data-min"));
        let value = qtyInput.val();

        if (value == minVal) {
            qtyInput.val(0).change();
            return;
        }
        if (value > minVal || isNaN(minVal)) {
            value--;
            qtyInput.val(value).change();
        }
    });

    //autoComplete
    const autoCompleteJS = new autoComplete({
        data: {
            src: async (query) => {
                if(!query.trim()){
                    return;
                }
                try {
                    const source = await fetch(route('shop.search', {term: query}), {
                        headers: {
                            'X-Requested-With' : 'XMLHttpRequest',
                            'Accept': 'application/json',
                        },
                    });
                    return await source.json();
                } catch (error) {
                    console.log(error);
                    return error;
                }
            },
            keys: ["title"],
            cache: true
        },
        resultItem: {
            element: (item, data) => {
                console.log(data.value.image)
                let html = `
                    <img src="${data.value.image}" style="max-width: 40px; margin: 0 10px" class='mr-2'>
                    <span>
                        ${data.match}
                    </span>
                `;
                item.style = "display: flex; justify-content: flex-start; align-items: center;";
                item.innerHTML = html;
            },
        },
    });

    autoCompleteJS.input.addEventListener("selection", function (event) {
        const feedback = event.detail;
        location.assign(feedback.selection.value.url);
    });

    ///checkout page
    ///////// ** select address ** /////////
    $(".adress-item>input").on("change", function () {
        if ($(this).is(":checked")) {
            var addressText = $.trim($(this).siblings(".address-text").text());
            $(".locationInput").val(addressText);
            $("#addressBook-modal").modal("hide");
        }
    });
    ///////// ** select time ** /////////
    $(".select-date").on("click", function () {
        if ($("input.select-date").is(":checked")) {
            $("#date-modal").modal("show");
        }
    });

    if ($(window).width() <= 991) {
        $(".register-section .user-ancor").removeAttr("data-toggle");
        $(".register-section .user-ancor").removeAttr("data-target");
    }

    $("#comment-link").on("click", function (e) {
        e.preventDefault();
        $("a[href=#tab-review]").click();
        $("html, body").animate(
            {
                scrollTop: $("#tab-review").offset().top - 100,
            },
            1000
        );
        $("#input-review").focus();
    });

    if ($(window).width() > 1199) {
        // $(".datePicker").flatpickr({
        //   locale: document.dir == "rtl" ? "ar" : "en",
        //   minDate: "today",
        //   dateFormat: "d M Y",
        //   defaultDate: "today",
        // });
    }
});
