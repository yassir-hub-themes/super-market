<?php
/**
 *sitemap
 *
 **/

use Illuminate\Support\Facades\DB;
use Tasawk\AppSettings\Setting\AppSettings;



Ecommerce::registerViews([
    'cart-total-summation' => 'theme::checkout.cart.total-summation'
]);

Payment::registerViews([
    'cancel' => 'theme::checkout.payment.cancel',
    'success' => 'theme::checkout.payment.success',
    'error' => 'theme::checkout.payment.error'
]);

$banners = function () {
    static $banners;
    if ($banners) {
        return $banners;
    }
    return $banners = DB::table('banners')
        ->select(['id'])
        ->get()
        ->pluck('id', 'id');
};
$data = [
    'title' => 'Super market Theme',
    'icon' => 'icon-browser',
    'group' => 'theme_super_market',
    'descriptions' => 'Control Ecommerce theme',
    'permission' => "manage_settings.landing_page",
    'sections' => [
        'general' => [
            'columns' => 2,
            'title' => 'General',
            'icon' => 'icon-cog3',
            'inputs' => [
                [
                    'type' => 'number',
                    'name' => 'pagination_count',
                    'label' => 'The number of products that will be shown per page',
                    'hint' => 'IF field left blank, the system consider 15 as default'

                ],
            ]
        ],
        'banners' => [
            'columns' => 2,
            'title' => 'Top banner',
            'icon' => 'icon-cog3',
            'inputs' => [
                [
                    'name' => 'html_block',
                    'type' => 'html',
                    "content" => view("theme::settings.top_banner"),
                    "html_inputs" => [
                        [
                            'name' => 'features',
                            'type' => 'repeatable',
                            "data_type" => "array",
                            'mutiple' => true,
                            'rules' => 'nullable',
                        ],
                    ],
                ],
            ]
        ],
        'Home' => [
            'columns' => 2,
            'title' => 'Home page',
            'icon' => 'icon-cog3',
            'inputs' => [
                [
                    'type' => 'select',
                    'name' => 'slider-position',
                    'label' => 'Slider position',
                    'options' => function () {
                        return DB::table('slider_positions as sp')
                            ->select(['sp.id', 'spt.title'])
                            ->join('slider_positions_translations as spt', 'sp.id', '=', 'spt.position_id')
                            ->where('spt.locale', app()->getLocale())
                            ->get()->pluck('title', 'id');
                    }
                ],
                [
                    'type' => 'select',
                    'name' => 'header_banner',
                    'label' => 'Header banner',
                    "hint" => 'the banner should be 624 x 663',
                    'options' => $banners
                ],
                [
                    'type' => 'select',
                    'name' => 'footer_banner',
                    'label' => 'Banner after recently arrived section',
                    "hint" => 'the banner should be 1884 x 232',
                    'options' => $banners
                ]
            ]
        ],
    ],
];

AppSettings::addPage('theme_super_market', $data);


